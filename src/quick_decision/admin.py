from django.contrib import admin
from quick_decision import forms
from quick_decision import models


@admin.register(models.DayTime)
class DayTimeAdmin(admin.ModelAdmin):
    form = forms.DayTimeForm


@admin.register(models.Sources)
class SourcesAdmin(admin.ModelAdmin):
    form = forms.SourcesForm


@admin.register(models.Variants)
class VariantsAdmin(admin.ModelAdmin):
    form = forms.VariantsForm


@admin.register(models.Var)
class VarAdmin(admin.ModelAdmin):
    form = forms.VarForm