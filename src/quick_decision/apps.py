from django.apps import AppConfig


class QuickDecisionConfig(AppConfig):
    name = 'quick_decision'
