from django.db import models
from django.utils.translation import gettext as _
from projectx.models import Target


class DayTime(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('DayTime In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('DayTime In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        unique=True,
        blank=True,
        verbose_name=_('DayTime In English'),
    )

    value = models.IntegerField(verbose_name=_('Value'))

    def __str__(self):
        return self.name


class Sources(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Source In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Source In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        unique=True,
        blank=True,
        verbose_name=_('Source In English'),
    )

    def __str__(self):
        return self.name


class Variants(models.Model):
    name = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Variants In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Variants In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Variants In English'),
    )

    slug = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    image = models.ImageField(verbose_name='Image', blank=True, null=True)

    description_en = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In English')
    )

    description_ru = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In Russian'),
    )

    description = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In Ukraine'),
    )

    source = models.ForeignKey(
        to='Sources',
        verbose_name=_('Source'),
        on_delete=models.CASCADE,
    )

    target = models.ForeignKey(
        Target,
        verbose_name=_('Target'),
        on_delete=models.SET_NULL,
        null=True
    )

    day_time = models.ForeignKey(
        to='DayTime',
        verbose_name=_('DayTIme'),
        on_delete=models.SET_NULL,
        null=True
    )

    proteins = models.IntegerField(verbose_name=_('Proteins'), blank=True)

    carbohydrates = models.IntegerField(verbose_name=_('Carbohydrates'), blank=True)

    fats = models.IntegerField(verbose_name=_('Fats'), blank=True)

    calories = models.IntegerField(verbose_name=_('Calories'), blank=True)

    def __str__(self):
        return self.name


class Var(models.Model):
    variants = models.ManyToManyField(Variants, blank=True)

