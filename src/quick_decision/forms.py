from django import forms
from quick_decision import models
from transliterate.exceptions import LanguageDetectionError
import re
from transliterate import translit


def transliterate(text):
    pieces = str(re.sub('[\W]+', ' ', text)).lower().split(' ')
    result = []

    for piece in pieces:
        try:
            result.append(translit(piece, reversed=True))
        except LanguageDetectionError:
            result.append(piece)
    return '-'.join([r for r in result if r])


class DayTimeForm(forms.ModelForm):
    class Meta:
        model = models.DayTime
        fields = '__all__'


class SourcesForm(forms.ModelForm):
    class Meta:
        model = models.Sources
        fields = '__all__'


class VariantsForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.Variants
        fields = '__all__'


class VariantForm(forms.ModelForm):
    class Meta:
        model = models.Variants
        fields = '__all__'


class VarForm(forms.ModelForm):
    class Meta:
        model = models.Var
        fields = '__all__'
