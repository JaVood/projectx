from django.test import TestCase
from quick_decision.models import DayTime, Sources, Variants
from projectx.models import Target


class BaseTest(TestCase):
    def setUp(self):
        Target.objects.create(
            id="1",
            name="test",
        )

        DayTime.objects.create(
            id="1",
            name="test",
            name_ru="test2",
            name_en="test3",
            value="1",
        )

        Sources.objects.create(
            id="2",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        Variants.objects.create(
            id="5",
            name="test",
            name_ru="test2",
            name_en="test3",
            slug="test",
            description="Run it",
            description_ru="Run it",
            description_en="Run it",
            source_id="2",
            target_id="1",
            day_time_id="1",
            proteins="100",
            carbohydrates="100",
            fats="100",
            calories="100",
        )


class DayTimeModelTest(BaseTest):
    def test_recipe_portion_name(self):
        day_time = DayTime.objects.get(id=1)
        self.assertEqual(day_time.name, "test")

    def test_day_time_name_ru(self):
        day_time = DayTime.objects.get(id=1)
        self.assertEqual(day_time.name_ru, "test2")

    def test_day_time_name_en(self):
        day_time = DayTime.objects.get(id=1)
        self.assertEqual(day_time.name_en, "test3")

    def test_day_time_value(self):
        day_time = DayTime.objects.get(id=1)
        self.assertEqual(day_time.value, 1)


class SourcesModelTest(BaseTest):
    def test_sources_name(self):
        sources = Sources.objects.get(id=2)
        self.assertEqual(sources.name, "test")

    def test_sources_name_ru(self):
        sources = Sources.objects.get(id=2)
        self.assertEqual(sources.name_ru, "test2")

    def test_sources_name_en(self):
        sources = Sources.objects.get(id=2)
        self.assertEqual(sources.name_en, "test3")


class VariantsModelTest(BaseTest):
    def test_variants_name(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.name, "test")

    def test_variants_name_ru(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.name_ru, "test2")

    def test_variants_name_en(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.name_en, "test3")

    def test_variants_description(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.description, "Run it")

    def test_variants_description_ru(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.description_ru, "Run it")

    def test_variants_description_en(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.description_en, "Run it")

    def test_variants_slug(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.slug, "test")

    def test_variants_source(self):
        variants = Variants.objects.get(id=5)
        sources = Sources.objects.get(id=2)
        self.assertEqual(variants.source, sources)

    def test_variants_target(self):
        variants = Variants.objects.get(id=5)
        target = Target.objects.get(id=1)
        self.assertEqual(variants.target, target)

    def test_variants_day_time(self):
        variants = Variants.objects.get(id=5)
        day_time = DayTime.objects.get(id=1)
        self.assertEqual(variants.day_time, day_time)

    def test_variants_proteins(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.proteins, 100)

    def test_variants_calories(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.calories, 100)

    def test_variants_carbohydrates(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.carbohydrates, 100)

    def test_variants_fats(self):
        variants = Variants.objects.get(id=5)
        self.assertEqual(variants.fats, 100)