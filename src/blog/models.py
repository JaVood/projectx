from django.db import models
from django.utils.translation import gettext as _
from core.models import BaseModel
from django.contrib.auth.models import User


class ArticleCategory(BaseModel):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Category In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Category In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Category In English'),
    )

    slug = models.CharField(
        max_length=150,
        unique=True,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    image = models.ImageField(verbose_name=_('Image'), null=True)

    author = models.ForeignKey(User,
                               related_name=_('article_category'),
                               on_delete=models.SET_NULL,
                               null=True,
                               blank=True,
                               verbose_name=_('Author'),
                               )

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.name


class Comment(BaseModel):
    author = models.ForeignKey(User,
                               related_name=_('author'),
                               on_delete=models.CASCADE,
                               verbose_name=_('Author'),
                               )

    article = models.ForeignKey(to='Article',
                                related_name=_('article'),
                                on_delete=models.SET_NULL,
                                null=True,
                                verbose_name=_('Article'),
                                )

    comment = models.CharField(
        max_length=300,
        verbose_name=_('Comment'),
    )

    ban = models.BooleanField(default=False)

    def __str__(self):
        return self.author.username


class Article(BaseModel):
    name = models.CharField(
        max_length=150,
        verbose_name=_('Article Name In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Article Name In Russian'),
    )

    name_en = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Article Name In English'),
    )

    slug = models.CharField(
        max_length=150,
        unique=True,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    article_of_day = models.BooleanField(verbose_name=_('Article Of day'), default=False)

    author = models.ForeignKey(User,
                               related_name=_('article'),
                               on_delete=models.SET_NULL,
                               null=True,
                               verbose_name=_('Author'),
                               )

    category = models.ForeignKey(ArticleCategory,
                                 related_name=_('article'),
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 verbose_name=_('Category'),
                                 )

    description = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Description'),
    )

    description_ru = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Description Ru'),
    )

    description_en = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Description En'),
    )

    first_half = models.TextField(max_length=5000, verbose_name=_('First Half'), blank=True, null=True)

    second_half = models.TextField(max_length=5000, verbose_name=_('Second Half'), blank=True, null=True)

    first_half_ru = models.TextField(max_length=5000, verbose_name=_('First Half Ru'), blank=True, null=True)

    second_half_ru = models.TextField(max_length=5000, verbose_name=_('Second Half Ru'), blank=True, null=True)

    first_half_en = models.TextField(max_length=5000, verbose_name=_('First Half En'), blank=True, null=True)

    second_half_en = models.TextField(max_length=5000, verbose_name=_('Second Half En'), blank=True, null=True)

    main_image = models.ImageField(verbose_name=_('Main Image'))

    image_1 = models.ImageField(verbose_name=_('Image 1'), blank=True, null=True)

    image_2 = models.ImageField(verbose_name=_('Image 2'), blank=True, null=True)

    image_3 = models.ImageField(verbose_name=_('Image 3'), blank=True, null=True)

    likes = models.ManyToManyField(User, blank=True, verbose_name=_('Likes'), related_name=_('likes'))

    def get_comments(self):
        a = Comment.objects.filter(article_id=self.id)
        return a.count()

    def __str__(self):
        return self.name



