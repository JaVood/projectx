# Generated by Django 2.1.1 on 2019-01-01 19:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=64, verbose_name='Article Name In Ukraine')),
                ('name_ru', models.CharField(blank=True, max_length=64, verbose_name='Article Name In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='Article Name In English')),
                ('slug', models.CharField(blank=True, help_text="Don't touch it!", max_length=150, unique=True, verbose_name='Slug')),
                ('article_of_day', models.BooleanField(default=False, verbose_name='Article Of day')),
                ('description', models.CharField(blank=True, max_length=1000, verbose_name='Description')),
                ('description_ru', models.CharField(blank=True, max_length=1000, verbose_name='Description Ru')),
                ('description_en', models.CharField(blank=True, max_length=1000, verbose_name='Description En')),
                ('first_half', models.TextField(blank=True, max_length=5000, null=True, verbose_name='First Half')),
                ('second_half', models.TextField(blank=True, max_length=5000, null=True, verbose_name='Second Half')),
                ('first_half_ru', models.TextField(blank=True, max_length=5000, null=True, verbose_name='First Half Ru')),
                ('second_half_ru', models.TextField(blank=True, max_length=5000, null=True, verbose_name='Second Half Ru')),
                ('first_half_en', models.TextField(blank=True, max_length=5000, null=True, verbose_name='First Half En')),
                ('second_half_en', models.TextField(blank=True, max_length=5000, null=True, verbose_name='Second Half En')),
                ('main_image', models.ImageField(upload_to='', verbose_name='Main Image')),
                ('image_1', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Image 1')),
                ('image_2', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Image 2')),
                ('image_3', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Image 3')),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article', to=settings.AUTH_USER_MODEL, verbose_name='Author')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ArticleCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Category In Ukraine')),
                ('name_ru', models.CharField(max_length=64, unique=True, verbose_name='Category In Russian')),
                ('name_en', models.CharField(max_length=64, unique=True, verbose_name='Category In English')),
                ('slug', models.CharField(blank=True, help_text="Don't touch it!", max_length=150, unique=True, verbose_name='Slug')),
                ('image', models.ImageField(null=True, upload_to='', verbose_name='Image')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article_category', to=settings.AUTH_USER_MODEL, verbose_name='Author')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('comment', models.CharField(max_length=300, verbose_name='Comment')),
                ('ban', models.BooleanField(default=False)),
                ('article', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article', to='blog.Article', verbose_name='Article')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='author', to=settings.AUTH_USER_MODEL, verbose_name='Author')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='article',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article', to='blog.ArticleCategory', verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='article',
            name='likes',
            field=models.ManyToManyField(blank=True, related_name='likes', to=settings.AUTH_USER_MODEL, verbose_name='Likes'),
        ),
    ]
