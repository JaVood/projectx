from django import forms
from blog import models
from transliterate.exceptions import LanguageDetectionError
import re
from transliterate import translit


def transliterate(text):
    pieces = str(re.sub('[\W]+', ' ', text)).lower().split(' ')
    result = []

    for piece in pieces:
        try:
            result.append(translit(piece, reversed=True))
        except LanguageDetectionError:
            result.append(piece)
    return '-'.join([r for r in result if r])


class ArticleForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.Article
        fields = '__all__'


class ArticleCategoryForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.ArticleCategory
        fields = '__all__'


class CommentForm(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields = '__all__'
