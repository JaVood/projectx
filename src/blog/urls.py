from django.urls import re_path
from django.conf.urls import url
from . import views

urlpatterns = [
    re_path(r'^$', views.BlogView.as_view(), name='blog'),
    re_path(r'^ru/$', views.BlogRuView.as_view(), name='blog'),
    url(r"^(?P<category_slug>.[\w-]+[']*)/$", views.category, name='category'),
    url(r"^ru/(?P<category_slug>.[\w-]+[']*)/$", views.categoryru, name='categoryru'),
    re_path(r"^team-member/(?P<member_slug>.+)/$", views.team, name='team_member'),
    re_path(r"^ru/team-member/(?P<member_slug>.+)/$", views.teamru, name='team_member_ru'),
    url(r"^article/(?P<article_slug>.+)/$", views.article, name='article'),
    url(r"^ru/article/(?P<article_slug>.+)/$", views.articleru, name='articleru'),
]
