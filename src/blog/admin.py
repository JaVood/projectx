from django.contrib import admin
from blog import models
from blog import forms


@admin.register(models.Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['name', 'article_of_day']
    list_filter = ['author', 'category']
    form = forms.ArticleForm


@admin.register(models.ArticleCategory)
class ArticleCategoryAdmin(admin.ModelAdmin):
    form = forms.ArticleCategoryForm


@admin.register(models.Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'article']
    list_filter = ['author', 'article']
    form = forms.CommentForm

