from django.contrib.auth import get_user_model
from django.test import TestCase
from blog.models import Article, ArticleCategory, Comment
from projectx.models import UserStatus


class BaseTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username='user', password='qwerty123'
        )

        UserStatus.objects.create(
            id="1",
            name="test",
        )

        ArticleCategory.objects.create(
            id="1",
            name="test",
            name_ru="test2",
            name_en="test3",
            slug="test",
            author=self.user,
        )

        Article.objects.create(
            id="5",
            name="test",
            name_ru="test2",
            name_en="test3",
            slug="test",
            author=self.user,
            article_of_day=True,
            category_id="1",
            description="Run it",
            first_half="Run it",
            second_half="Run it",
        )

        Comment.objects.create(
            id="2",
            author=self.user,
            article_id="5",
            comment="test"
        )


class ArticleCategoryModelTest(BaseTest):
    def test_recipe_portion_name(self):
        article = ArticleCategory.objects.get(id=1)
        self.assertEqual(article.name, "test")

    def test_article_name_ru(self):
        article = ArticleCategory.objects.get(id=1)
        self.assertEqual(article.name_ru, "test2")

    def test_article_name_en(self):
        article = ArticleCategory.objects.get(id=1)
        self.assertEqual(article.name_en, "test3")

    def test_article_slug(self):
        article = ArticleCategory.objects.get(id=1)
        self.assertEqual(article.slug, "test")

    def test_article_author(self):
        article = ArticleCategory.objects.get(id=1)
        self.assertEqual(article.author, self.user)


class CommentModelTest(BaseTest):
    def test_comment_author(self):
        comment = Comment.objects.get(id=2)
        self.assertEqual(comment.author, self.user)

    def test_comment_article(self):
        comment = Comment.objects.get(id=2)
        article = Article.objects.get(id=5)
        self.assertEqual(comment.article, article)

    def test_comment_comment(self):
        comment = Comment.objects.get(id=2)
        self.assertEqual(comment.comment, "test")


class ArticleModelTest(BaseTest):
    def test_recipe_name(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.name, "test")

    def test_article_name_ru(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.name_ru, "test2")

    def test_article_name_en(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.name_en, "test3")

    def test_article_description(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.description, "Run it")

    def test_article_first_half(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.first_half, "Run it")

    def test_article_second_half(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.second_half, "Run it")

    def test_article_slug(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.slug, "test")

    def test_article_author(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.author, self.user)

    def test_article_category(self):
        article = Article.objects.get(id=5)
        article_category = ArticleCategory.objects.get(id=1)
        self.assertEqual(article.category, article_category)

    def test_article_of_day(self):
        article = Article.objects.get(id=5)
        self.assertEqual(article.article_of_day, True)
