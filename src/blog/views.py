from core.views import BaseView
from .models import Article, ArticleCategory, Comment
from .forms import CommentForm
from projectx.models import Profile
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Count
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404, JsonResponse
from django.template.loader import render_to_string


class BlogView(BaseView):
    template_name = 'blog/blog.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        articles = Article.objects.all().order_by('-id')
        popular = Article.objects.all().annotate(count=Count('likes')).order_by('-count')[:2]
        categories = ArticleCategory.objects.all()
        page = request.GET.get('page')
        paginator_article = Paginator(articles, 5)
        try:
            article = paginator_article.page(page)
        except PageNotAnInteger:
            article = paginator_article.page(1)
        except EmptyPage:
            article = paginator_article.page(paginator_article.num_pages)

        context.update({
            'articles': article,
            'popular': popular,
            'categories': categories,
        })
        return self.render_to_response(context)


def category(request, category_slug):
    category = ArticleCategory.objects.get(slug=category_slug)
    articles = Article.objects.filter(category_id=category.id).order_by('-id')
    popular = Article.objects.filter(category_id=category.id).annotate(count=Count('likes')).order_by('-count')[:2]
    categories = ArticleCategory.objects.all()
    page = request.GET.get('page')
    paginator_article = Paginator(articles, 5)
    try:
        article = paginator_article.page(page)
    except PageNotAnInteger:
        article = paginator_article.page(1)
    except EmptyPage:
        article = paginator_article.page(paginator_article.num_pages)
    return render(request, 'blog/blog-category.html', {'articles': article,
                                                       'popular': popular,
                                                       'categories': categories,
                                                       'category': category,
                                                       })


@csrf_exempt
def article(request, article_slug):
    article = Article.objects.get(slug=article_slug)
    author = article.author
    more = Article.objects.filter(author_id=author.id)[:3]
    comments = Comment.objects.filter(article_id=article.id).order_by('created')
    for i in comments:
        if i.ban:
            comments = comments.exclude(id=i.id)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('blog:article', article_slug=article.slug)
    else:
        form = CommentForm()
    user = request.user
    context = {'article': article,
               'more': more,
               'comments': comments,
               'form': form,
               'user': user,
               }
    if request.is_ajax():
        rendered = render_to_string('blog/article-2.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'blog/article.html', context)


def team(request, member_slug):
    profile = Profile.objects.get(slug=member_slug)
    return render(request, 'blog/team_member.html', {'profile': profile, })


class BlogRuView(BaseView):
    template_name = 'blog/blog-ru.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        articles = Article.objects.all().order_by('-id')
        popular = Article.objects.all().annotate(count=Count('likes')).order_by('-count')[:2]
        categories = ArticleCategory.objects.all()
        page = request.GET.get('page')
        paginator_article = Paginator(articles, 5)
        try:
            article = paginator_article.page(page)
        except PageNotAnInteger:
            article = paginator_article.page(1)
        except EmptyPage:
            article = paginator_article.page(paginator_article.num_pages)

        context.update({
            'articles': article,
            'popular': popular,
            'categories': categories,
        })
        return self.render_to_response(context)


def categoryru(request, category_slug):
    category = ArticleCategory.objects.get(slug=category_slug)
    articles = Article.objects.filter(category_id=category.id).order_by('-id')
    popular = Article.objects.filter(category_id=category.id).annotate(count=Count('likes')).order_by('-count')[:2]
    categories = ArticleCategory.objects.all()
    page = request.GET.get('page')
    paginator_article = Paginator(articles, 5)
    try:
        article = paginator_article.page(page)
    except PageNotAnInteger:
        article = paginator_article.page(1)
    except EmptyPage:
        article = paginator_article.page(paginator_article.num_pages)
    return render(request, 'blog/blog-category-ru.html', {'articles': article,
                                                       'popular': popular,
                                                       'categories': categories,
                                                       'category': category,
                                                       })


@csrf_exempt
def articleru(request, article_slug):
    article = Article.objects.get(slug=article_slug)
    author = article.author
    more = Article.objects.filter(author_id=author.id)[:3]
    comments = Comment.objects.filter(article_id=article.id).order_by('created')
    for i in comments:
        if i.ban:
            comments = comments.exclude(id=i.id)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('blog:articleru', article_slug=article.slug)
    else:
        form = CommentForm()
    user = request.user
    context = {'article': article,
               'more': more,
               'comments': comments,
               'form': form,
               'user': user,
               }
    if request.is_ajax():
        rendered = render_to_string('blog/article-2-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'blog/article-ru.html', context)


def teamru(request, member_slug):
    profile = Profile.objects.get(slug=member_slug)
    return render(request, 'blog/team_member-ru.html', {'profile': profile, })
