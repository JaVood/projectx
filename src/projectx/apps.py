from django.apps import AppConfig
from watson import search as watson


class ProjectxConfig(AppConfig):
    name = 'projectx'

    def ready(self):
        Profile = self.get_model("Profile")
        watson.register(Profile)
