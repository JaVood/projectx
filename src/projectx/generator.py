def generate_diet(user):
    from projectx.models import Diet

    # count boo and body index
    diet = Diet.objects.get(id=user.diet_id)

    if diet.meals_per_day is not None:
        meals_check = diet.meals_per_day
    else:
        meals_check = 0

    if user.sex_id is 1:
        if user.age <= 18:
            diet.boo = user.weight * 24.5
        elif user.age >= 19 and user.age <= 30:
            diet.boo = user.weight * 24
        elif user.age >= 31 and user.age <= 60:
            diet.boo = user.weight * 23
        elif user.age >= 61:
            diet.boo = user.weight * 22
    else:
        if user.age <= 18:
            diet.boo = user.weight * 22.5
        elif user.age >= 19 and user.age <= 30:
            diet.boo = user.weight * 22
        elif user.age >= 31 and user.age <= 60:
            diet.boo = user.weight * 21
        elif user.age >= 61:
            diet.boo = user.weight * 20

    # count day calories norm
    if user.life_style_id is 1:
        diet.day_calories_norm = diet.boo
    elif user.life_style_id is 2:
        diet.day_calories_norm = diet.boo * 1.2
    elif user.life_style_id is 3:
        diet.day_calories_norm = diet.boo * 1.25
    elif user.life_style_id is 4:
        diet.day_calories_norm = diet.boo * 1.35
    elif user.life_style_id is 5:
        diet.day_calories_norm = diet.boo * 1.45
    elif user.life_style_id is 6:
        diet.day_calories_norm = diet.boo * 1.6

    # include body type
    if user.body_type_id is 1:
        diet.day_calories_norm += 100
    elif user.body_type_id is 3 and user.target_id is 1:
        diet.day_calories_norm -= 100
    elif user.body_type_id is 3 and user.target_id is 2:
        diet.day_calories_norm -= 300

    # count day calories target
    if user.target_id is 2:
        diet.day_target_calories = diet.day_calories_norm + 200
    elif user.target_id is 1:
        diet.day_target_calories = diet.day_calories_norm - 200
    else:
        diet.day_target_calories = diet.day_calories_norm

    # count day proteins, carbohydrates, fats:
    diet.proteins = (diet.day_target_calories * 0.3) / 4.5
    diet.carbohydrates = (diet.day_target_calories * 0.5) / 4.5
    diet.fats = (diet.day_target_calories * 0.2) / 9

    # count meals per day:
    if diet.day_target_calories < 1500:
        diet.meals_per_day = 3
    elif diet.day_target_calories >= 1500 and diet.day_target_calories < 2500:
        diet.meals_per_day = 4
    elif diet.day_target_calories >= 2500 and diet.day_target_calories < 3000:
        diet.meals_per_day = 5
    elif diet.day_target_calories >= 3000 and diet.day_target_calories < 3500:
        diet.meals_per_day = 6
    elif diet.day_target_calories >= 3500 and diet.day_target_calories < 4000:
        diet.meals_per_day = 7
    elif diet.day_target_calories >= 4000:
        diet.meals_per_day = 8
    diet.water = user.weight * 30
    diet.day_target_calories = diet.day_target_calories + diet.adaptation
    diet.save()


def count_proteins(profile, recipe):
    from projectx.models import Profile, Meals
    from recipe.models import FSRecipe
    from datetime import date

    profile = Profile.objects.get(id=profile.id)
    recipe = FSRecipe.objects.get(id=recipe.id)

    meals_finish = Meals.objects.filter(user_id=profile.user.id, created__date=date.today())

    proteins_consumed = 0

    meals_count = 0

    for i in meals_finish:
        proteins_consumed += i.proteins
        if i.fsrecipe and i.fsrecipe.recipe_category_id == 1:
            meals_count += 1
    proteins = 0
    if (profile.diet.meals_per_day - meals_count) > 0:
        proteins = (profile.diet.proteins - proteins_consumed) / (profile.diet.meals_per_day - meals_count)

    if proteins > 0:
        portion = proteins / recipe.proteins
    else:
        portion = 0

    return portion


def count_carbo(profile, recipe):
    from projectx.models import Profile, Meals
    from recipe.models import FSRecipe
    from datetime import date

    profile = Profile.objects.get(id=profile.id)
    recipe = FSRecipe.objects.get(id=recipe.id)

    meals_finish = Meals.objects.filter(user_id=profile.user.id, created__date=date.today())

    carbo_consumed = 0

    meals_count = 0

    for i in meals_finish:
        carbo_consumed += i.carbohydrates
        if i.fsrecipe and i.fsrecipe.recipe_category_id == 2:
            meals_count += 1

    carbo = 0
    if profile.target_id != 2:
        if (profile.diet.meals_per_day - meals_count - 1) > 0:
            carbo = (profile.diet.carbohydrates - carbo_consumed) / (profile.diet.meals_per_day - meals_count - 1)
    else:
        if (profile.diet.meals_per_day - meals_count) > 0:
            carbo = (profile.diet.carbohydrates - carbo_consumed) / (profile.diet.meals_per_day - meals_count)

    if carbo > 0:
        portion = carbo / recipe.carbohydrates
    else:
        portion = 0

    return portion


