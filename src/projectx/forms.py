from django import forms
from projectx import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms.widgets import PasswordInput, TextInput
from transliterate.exceptions import LanguageDetectionError
import re
from transliterate import translit
from PIL import Image
from .models import Profile
from recipe.models import Recipe


def transliterate(text):
    pieces = str(re.sub('[\W]+', ' ', text)).lower().split(' ')
    result = []

    for piece in pieces:
        try:
            result.append(translit(piece, reversed=True))
        except LanguageDetectionError:
            result.append(piece)
    return '-'.join([r for r in result if r])


class UserForm(forms.ModelForm):
    class Meta:
        model = models.User
        fields = ('first_name', 'last_name', 'email',)


class ProfileForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name_slug'])

    class Meta:
        model = models.Profile
        fields = '__all__'


class SexForm(forms.ModelForm):
    class Meta:
        model = models.Sex
        fields = '__all__'


class PortionNumberForm(forms.ModelForm):
    class Meta:
        model = models.PortionNumber
        fields = '__all__'


class BodyTypeForm(forms.ModelForm):
    class Meta:
        model = models.BodyType
        fields = '__all__'


class LifeStyleForm(forms.ModelForm):
    class Meta:
        model = models.LifeStyle
        fields = '__all__'


class TargetForm(forms.ModelForm):
    class Meta:
        model = models.Target
        fields = '__all__'


class DietForm(forms.ModelForm):
    class Meta:
        model = models.Diet
        fields = '__all__'


class IndexForm(forms.ModelForm):
    class Meta:
        model = models.Index
        fields = '__all__'


class MealsPerDayNameForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['slug'])

    class Meta:
        model = models.MealsPerDayName
        fields = '__all__'


class MealsPerDayForm(forms.ModelForm):
    class Meta:
        model = models.MealsPerDay
        fields = '__all__'


class MealsForm(forms.ModelForm):
    class Meta:
        model = models.Meals
        fields = '__all__'


class MealsDeleteForm(forms.ModelForm):
    class Meta:
        model = models.Meals
        fields = ('user', 'meals_per_day', 'recipe',)


class SignUpForm(forms.Form):
    first_name = forms.CharField(widget=TextInput(attrs={"placeholder": "Ім'я"}), min_length=3, max_length=150)
    last_name = forms.CharField(widget=TextInput(attrs={'placeholder': 'Прізвище'}), min_length=3, max_length=150)
    username = forms.CharField(widget=TextInput(attrs={'placeholder': 'Нікнейм(на анг)'}), min_length=3, max_length=150)
    email = forms.EmailField(widget=TextInput(attrs={'placeholder': 'Електронний адрес'}), max_length=254)
    password1 = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Пароль'}), min_length=6)
    password2 = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Підтвердження паролю'}), min_length=6)

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("Username already exists")
        return username

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")

        return password2

    def save(self, commit=True):
        user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password1'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
        )
        return user

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2', )


class SignUpFormRu(forms.Form):
    first_name = forms.CharField(widget=TextInput(attrs={'placeholder': 'Имя'}), min_length=3, max_length=150)
    last_name = forms.CharField(widget=TextInput(attrs={'placeholder': 'Фамилия'}), min_length=3, max_length=150)
    username = forms.CharField(widget=TextInput(attrs={'placeholder': 'NickName(на анг)'}), min_length=3, max_length=150)
    email = forms.EmailField(widget=TextInput(attrs={'placeholder': 'Электронный адрес'}), max_length=254)
    password1 = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Пароль'}), min_length=6)
    password2 = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Подтверждение пароля'}), min_length=6)

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("Username already exists")
        return username

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")

        return password2

    def save(self, commit=True):
        user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password1'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
        )
        return user

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2', )


class PhotoForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model = Profile
        fields = ('file', 'x', 'y', 'width', 'height', )
        widgets = {
            'file': forms.FileInput(attrs={
                'accept': 'image/*'  # this is not an actual validation! don't rely on that!
            })
        }

    def save(self):
        photo = super(PhotoForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.file)
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((400, 400), Image.ANTIALIAS)
        resized_image.save(photo.file.path)

        return photo


class DeleteRecipeForm(forms.ModelForm):
    delete_recipe = forms.ModelMultipleChoiceField(queryset=Recipe.objects.all())

    class Meta:
        model = Profile
        fields = ('delete_recipes',)


class SavedRecipeForm(forms.ModelForm):
    recipes = forms.ModelMultipleChoiceField(queryset=Recipe.objects.all())

    class Meta:
        model = Profile
        fields = ('recipes',)


class AddFriendForm(forms.ModelForm):
    friends = forms.ModelMultipleChoiceField(queryset=User.objects.all())

    class Meta:
        model = Profile
        fields = ('friends', 'name', )


class RemoveFriendForm(forms.ModelForm):
    friend_request = forms.ModelMultipleChoiceField(queryset=User.objects.all())

    class Meta:
        model = Profile
        fields = ('friend_request',)


class FindFriendForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('name',)


class UserStatusForm(forms.ModelForm):
    class Meta:
        model = models.UserStatus
        fields = '__all__'


class SettingMainForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('life_style', 'target', 'weight', )


class BonusForm(forms.ModelForm):
    class Meta:
        model = models.Bonus
        fields = '__all__'


class AboutForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('status', 'facebook', 'instagram', 'country', 'city', 'bonus',)


