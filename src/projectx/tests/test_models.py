from django.contrib.auth import get_user_model
from django.test import TestCase
from projectx.models import UserStatus, Sex, LifeStyle, BodyType, Index, Diet, Profile, Target, Meals, MealsPerDay, MealsPerDayName, PortionNumber, Bonus
from recipe.models import RecipeDayTime, Recipe, RecipePortionData, RecipeDifficult, RecipeCategory


class BaseTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username='user', password='qwerty123'
        )

        UserStatus.objects.create(
            id="1",
            name="test",
        )

        Bonus.objects.create(
            id="1",
            user=self.user,
            value="test",
        )

        PortionNumber.objects.create(
            id="1",
            name="test",
            value="100",
        )

        Sex.objects.create(
            id="1",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        RecipeDayTime.objects.create(
            id="2",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        LifeStyle.objects.create(
            id="2",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        Target.objects.create(
            id="2",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        MealsPerDay.objects.create(
            id="2",
            name="test",
            value="100",
        )

        MealsPerDayName.objects.create(
            id="2",
            name="test",
            name_ru="test2",
            name_en="test3",
            value="100",
            day_time_id="20"
        )

        BodyType.objects.create(
            id="4",
            name="test",
            name_ru="test2",
            name_en="test3",
            description="test",
            description_ru="test",
            description_en="test",
        )

        Index.objects.create(
            id="4",
            name="test",
            name_ru="test2",
            name_en="test3",
            description="test",
            description_ru="test",
            description_ua="test",
        )

        Diet.objects.create(
            id="400",
            boo="100",
            day_calories_norm="100",
            day_target_calories="100",
            proteins="100",
            carbohydrates="100",
            fats="100",
            water="100",
            meals_per_day="100",
        )

        RecipePortionData.objects.create(
            id="10",
            name="test1",
            name_ru="test21",
            name_en="test31"
        )

        RecipeDayTime.objects.create(
            id="20",
            name="test1",
            name_ru="test21",
            name_en="test31"
        )

        RecipeDifficult.objects.create(
            id="30",
            name="test1",
            name_ru="test21",
            name_en="test31"
        )

        RecipeCategory.objects.create(
            id="40",
            name="test1",
            name_ru="test21",
            name_en="test31",
            description="test",
            description_ru="test",
            description_en="test",
            slug="test1",
        )

        Recipe.objects.create(
            id="10",
            name="test1",
            name_ru="test21",
            name_en="test31",
            slug="test1",
            author=self.user,
            description="Run it",
            description_ru="Run it",
            description_en="Run it",
            recipe_category_id="40",
            products="products",
            products_ru="products",
            products_en="products",
            cooking_method="cooking",
            cooking_method_ru="cooking",
            cooking_method_en="cooking",
            difficult_id="30",
            day_time_id="20",
            portion="100",
            portion_data_id="10",
            proteins="100",
            carbohydrates="100",
            fats="100",
            calories="100",
            likes="100",
            water="100"
        )

        Meals.objects.create(
            id="4",
            user=self.user,
            meals_per_day_id="2",
            portion_number_id="1",
            recipe_id="10",
            proteins="100",
            carbohydrates="100",
            fats="100",
            water="100",
            calories="100",
        )

        profile = Profile.objects.get(user=self.user)
        profile.referral_code = "1"
        profile.phone = "1"
        profile.status = "test"
        profile.name = "user"
        profile.first_name = "user"
        profile.last_name = "user"
        profile.description_en = "user"
        profile.description = "user"
        profile.description_ru = "user"
        profile.facebook = "https://www.facebook.com/nazar.gesyk?ref=bookmarks"
        profile.instagram = "https://www.facebook.com/nazar.gesyk?ref=bookmarks"
        profile.you_tube = "https://www.facebook.com/nazar.gesyk?ref=bookmarks"
        profile.sex_id = 1
        profile.age = 100
        profile.height = 100
        profile.start_weight = 100
        profile.weight = 100
        profile.target_id = 2
        profile.life_style_id = 2
        profile.body_type_id = 4
        profile.country = "test"
        profile.city = "test"
        profile.save()


class PortionNumberModelTest(BaseTest):
    def test_recipe_portion_name(self):
        portion = PortionNumber.objects.get(id=1)
        self.assertEqual(portion.name, "test")

    def test_portion_value(self):
        portion = PortionNumber.objects.get(id=1)
        self.assertEqual(portion.value, 100)


class BonusModelTest(BaseTest):
    def test_bonus_user(self):
        portion = Bonus.objects.get(id=1)
        self.assertEqual(portion.user, self.user)

    def test_bonus_value(self):
        portion = Bonus.objects.get(id=1)
        self.assertEqual(portion.value, "test")


class SexModelTest(BaseTest):
    def test_recipe_portion_name(self):
        sex = Sex.objects.get(id=1)
        self.assertEqual(sex.name, "test")

    def test_sex_name_ru(self):
        sex = Sex.objects.get(id=1)
        self.assertEqual(sex.name_ru, "test2")

    def test_sex_name_en(self):
        sex = Sex.objects.get(id=1)
        self.assertEqual(sex.name_en, "test3")


class LifeStyleModelTest(BaseTest):
    def test_recipe_day_time_name(self):
        life_style = LifeStyle.objects.get(id=2)
        self.assertEqual(life_style.name, "test")

    def test_life_style_name_ru(self):
        life_style = LifeStyle.objects.get(id=2)
        self.assertEqual(life_style.name_ru, "test2")

    def test_life_style_name_en(self):
        life_style = LifeStyle.objects.get(id=2)
        self.assertEqual(life_style.name_en, "test3")


class MealsPerDayNameModelTest(BaseTest):
    def test_meals_per_day_name_name(self):
        meals_per_day_name = MealsPerDayName.objects.get(id=2)
        self.assertEqual(meals_per_day_name.name, "test")

    def test_meals_per_day_name_ru(self):
        meals_per_day = MealsPerDayName.objects.get(id=2)
        self.assertEqual(meals_per_day.name_ru, "test2")

    def test_meals_per_day_name_en(self):
        meals_per_day = MealsPerDayName.objects.get(id=2)
        self.assertEqual(meals_per_day.name_en, "test3")

    def test_meals_per_day_value(self):
        meals_per_day = MealsPerDayName.objects.get(id=2)
        self.assertEqual(meals_per_day.value, 100)

    def test_meals_per_day_day_time(self):
        meals_per_day = MealsPerDayName.objects.get(id=2)
        day_time = RecipeDayTime.objects.get(id=20)
        self.assertEqual(meals_per_day.day_time, day_time)


class BodyTypeModelTest(BaseTest):
    def test_body_type_name(self):
        body_type = BodyType.objects.get(id=4)
        self.assertEqual(body_type.name, "test")

    def test_body_type_name_ru(self):
        body_type = BodyType.objects.get(id=4)
        self.assertEqual(body_type.name_ru, "test2")

    def test_body_type_name_en(self):
        body_type = BodyType.objects.get(id=4)
        self.assertEqual(body_type.name_en, "test3")

    def test_body_type_description(self):
        body_type = BodyType.objects.get(id=4)
        self.assertEqual(body_type.description, "test")

    def test_body_type_description_ru(self):
        body_type = BodyType.objects.get(id=4)
        self.assertEqual(body_type.description_ru, "test")

    def test_body_type_description_en(self):
        body_type = BodyType.objects.get(id=4)
        self.assertEqual(body_type.description_en, "test")


class IndexModelTest(BaseTest):
    def test_index_name(self):
        index = Index.objects.get(id=4)
        self.assertEqual(index.name, "test")

    def test_index_name_ru(self):
        index = Index.objects.get(id=4)
        self.assertEqual(index.name_ru, "test2")

    def test_index_name_en(self):
        index = Index.objects.get(id=4)
        self.assertEqual(index.name_en, "test3")

    def test_index_description(self):
        index = Index.objects.get(id=4)
        self.assertEqual(index.description, "test")

    def test_index_description_ru(self):
        index = Index.objects.get(id=4)
        self.assertEqual(index.description_ru, "test")

    def test_index_description_ua(self):
        index = Index.objects.get(id=4)
        self.assertEqual(index.description_ua, "test")


class TargetModelTest(BaseTest):
    def test_target_name(self):
        target = Target.objects.get(id=2)
        self.assertEqual(target.name, "test")

    def test_target_name_ru(self):
        target = Target.objects.get(id=2)
        self.assertEqual(target.name_ru, "test2")

    def test_target_name_en(self):
        target = Target.objects.get(id=2)
        self.assertEqual(target.name_en, "test3")


class MealsPerDayModelTest(BaseTest):
    def test_name_name(self):
        meals_per_day = MealsPerDay.objects.get(id=2)
        self.assertEqual(meals_per_day.name, "test")

    def test_target_value(self):
        meals_per_day = MealsPerDay.objects.get(id=2)
        self.assertEqual(meals_per_day.value, 100)


class DietModelTest(BaseTest):
    def test_diet_boo(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.boo, 100)

    def test_diet_day_calories_norm(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.day_calories_norm, 100)

    def test_diet_proteins(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.proteins, 100)

    def test_diet_carbohydrates(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.carbohydrates, 100)

    def test_diet_fats(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.fats, 100)

    def test_diet_water(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.water, 100)

    def test_diet_meals_per_day(self):
        diet = Diet.objects.get(id=400)
        self.assertEqual(diet.meals_per_day, 100)


class MealsModelTest(BaseTest):
    def test_meals_user(self):
        meals = Meals.objects.get(id=4)
        self.assertEqual(meals.user, self.user)

    def test_meals_meals_per_day(self):
        meals = Meals.objects.get(id=4)
        meals_per_day = MealsPerDayName.objects.get(id=2)
        self.assertEqual(meals.meals_per_day, meals_per_day)

    def test_meals_portion_number(self):
        meals = Meals.objects.get(id=4)
        portion_number = PortionNumber.objects.get(id=1)
        self.assertEqual(meals.portion_number, portion_number)

    def test_meals_recipe(self):
        meals = Meals.objects.get(id=4)
        recipe = Recipe.objects.get(id=10)
        self.assertEqual(meals.recipe, recipe)

    def test_meals_day_calories(self):
        meals = Meals.objects.get(id=4)
        self.assertEqual(meals.calories, 100)

    def test_meals_proteins(self):
        meals = Meals.objects.get(id=4)
        self.assertEqual(meals.proteins, 100)

    def test_meals_carbohydrates(self):
        meals = Meals.objects.get(id=4)
        self.assertEqual(meals.carbohydrates, 100)

    def test_meals_fats(self):
        meals = Meals.objects.get(id=4)
        self.assertEqual(meals.fats, 100)

    def test_meals_water(self):
        meals = Meals.objects.get(id=4)
        self.assertEqual(meals.water, 100)


class ProfileModelTest(BaseTest):
    def test_profile_name(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.name, "user")

    def test_profile_referral_code(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.referral_code, "1")

    def test_profile_phone(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.phone, "1")

    def test_profile_status(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.status, "test")

    def test_profile_first_name(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.first_name, "user")

    def test_profile_last_name(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.last_name, "user")

    def test_profile_description_en(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.description_en, "user")

    def test_profile_description_ru(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.description_ru, "user")

    def test_profile_description(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.description, "user")

    def test_profile_facebook(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.facebook, "https://www.facebook.com/nazar.gesyk?ref=bookmarks")

    def test_profile_instagram(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.instagram, "https://www.facebook.com/nazar.gesyk?ref=bookmarks")

    def test_profile_you_tube(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.you_tube, "https://www.facebook.com/nazar.gesyk?ref=bookmarks")

    def test_profile_sex(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.sex_id, 1)

    def test_profile_age(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.age, 100)

    def test_profile_height(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.height, 100)

    def test_profile_weight(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.weight, 100)

    def test_profile_start_weight(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.start_weight, 100)

    def test_profile_target(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.target_id, 2)

    def test_profile_life_style(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.life_style_id, 2)

    def test_profile_body_type(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.body_type_id, 4)

    def test_profile_country(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.country, "test")

    def test_profile_city(self):
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.city, "test")
