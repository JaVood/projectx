
from core.views import BaseView
from django.contrib.auth import login, authenticate
from projectx.forms import SignUpForm, ProfileForm, SignUpFormRu
from projectx.models import Profile, Meals, Diet, LifeStyle, Target, Bonus, Sex, BodyType
from blog.models import Article
from .forms import PhotoForm, MealsForm, DeleteRecipeForm, \
    SavedRecipeForm, MealsDeleteForm, AddFriendForm, RemoveFriendForm, FindFriendForm, SettingMainForm, AboutForm
from quick_decision.forms import VariantForm
from quick_decision.models import DayTime, Sources, Variants, Var
from recipe.models import Recipe, RecipeCategory, FSRecipe, FSRecipeCategory
from projectx.models import MealsPerDay, MealsPerDayName
from datetime import date, timedelta
from django.shortcuts import render, get_object_or_404, redirect
from transliterate.exceptions import LanguageDetectionError
import re
from transliterate import translit
import online_users.models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django_private_chat.models import Dialog
from watson import search as watson
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from .liqpay import LiqPay
from django.http import HttpResponse
from core import settings
from django.views.generic import View
from django.utils.decorators import method_decorator
from time import gmtime, strftime
from dateutil.relativedelta import relativedelta
import datetime
from django.utils import timezone
import pytz
from .generator import count_proteins, count_carbo


def see_users(self):

  user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
  users = (user for user in user_status)
  context = {"online_users"}


def transliterate(text):
    pieces = str(re.sub('[\W]+', ' ', text)).lower().split(' ')
    result = []

    for piece in pieces:
        try:
            result.append(translit(piece, reversed=True))
        except LanguageDetectionError:
            result.append(piece)
    return '-'.join([r for r in result if r])


class IndexView(BaseView):
    template_name = 'projectx/index.html'


class AboutView(BaseView):
    template_name = 'projectx/about.html'


class ContactView(BaseView):
    template_name = 'projectx/contact.html'


class PrivateView(BaseView):
    template_name = 'projectx/private.html'


class ConditionsView(BaseView):
    template_name = 'projectx/conditions.html'


class PrivateRuView(BaseView):
    template_name = 'projectx/private-ru.html'


class ConditionsRuView(BaseView):
    template_name = 'projectx/conditions-ru.html'


def profile(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    article = Article.objects.filter(article_of_day=True)
    for i in article:
        article = i
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    status = False
    friend = Profile.objects.order_by("?")[:3]
    for userv in user_status:
        if userv.user == profile.user:
            status = True
    history = profile.history.all()
    weight = []
    time = []
    for i in history:
        if i.prev_record:
            if i.prev_record.weight != i.weight:
                weight.append(i.weight)
                time.append(i.history_date)
    weight.reverse()
    time.reverse()
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            for i in friend:
                i.profile.friend_request.add(profile.user_id)
                i.save()
            profile.save()
            return redirect('projectx:profile', profile_slug=profile.slug)
    else:
        form = AddFriendForm()
    return render(request, 'projectx/profile-main.html', {'form': form,
                                                          'profile': profile,
                                                          'friend_request': friend_request,
                                                          'messages': messages,
                                                          'status': status,
                                                          'article': article,
                                                          'friends': friend,
                                                          'weight': weight,
                                                          'time': time,
                                                          'created': profile.created,
                                                          })


@csrf_exempt
def profile_diet(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    diet = Diet.objects.get(id=profile.diet.id)
    friend_request = profile.friend_request.all().order_by('id')
    recipe = Recipe.objects.filter(recipe_category_id=1)
    profile_recipe_deleted = profile.delete_recipes.all()
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all().order_by('id')
    meals_finish = Meals.objects.filter(user_id=user.id, created__date=date.today())
    consumed = 0
    protein_consumed = 0
    carbo_consumed = 0
    fat_consumed = 0
    water_consumed = 0
    one = set()
    two = set()
    three = set()
    four = set()
    five = set()
    six = set()
    seven = set()
    eight = set()
    fsone = set()
    fstwo = set()
    fsthree = set()
    fsfour = set()
    fsfive = set()
    fssix = set()
    fsseven = set()
    fseight = set()
    p = FSRecipeCategory.objects.get(id=1)
    c = FSRecipeCategory.objects.get(id=2)
    d = FSRecipeCategory.objects.get(id=3)
    dd = FSRecipeCategory.objects.get(id=4)
    for i in recipe:
        for j in profile_recipe_deleted:
            if i == j:
                recipe = recipe.exclude(id=j.id)
    for i in meals_finish:
        consumed += i.calories
        protein_consumed += i.proteins
        carbo_consumed += i.carbohydrates
        fat_consumed += i.fats
        water_consumed += i.water
        if i.recipe and i.recipe.name_en != ('Water'):
            if i.meals_per_day_id == 1:
                one.add(i.recipe)
            elif i.meals_per_day_id == 2:
                two.add(i.recipe)
            elif i.meals_per_day_id == 3:
                three.add(i.recipe)
            elif i.meals_per_day_id == 4:
                four.add(i.recipe)
            elif i.meals_per_day_id == 5:
                five.add(i.recipe)
            elif i.meals_per_day_id == 6:
                six.add(i.recipe)
            elif i.meals_per_day_id == 7:
                seven.add(i.recipe)
            elif i.meals_per_day_id == 8:
                eight.add(i.recipe)
        if i.fsrecipe:
            if i.meals_per_day_id == 1:
                fsone.add(i.fsrecipe)
            elif i.meals_per_day_id == 2:
                fstwo.add(i.fsrecipe)
            elif i.meals_per_day_id == 3:
                fsthree.add(i.fsrecipe)
            elif i.meals_per_day_id == 4:
                fsfour.add(i.fsrecipe)
            elif i.meals_per_day_id == 5:
                fsfive.add(i.fsrecipe)
            elif i.meals_per_day_id == 6:
                fssix.add(i.fsrecipe)
            elif i.meals_per_day_id == 7:
                fsseven.add(i.fsrecipe)
            elif i.meals_per_day_id == 8:
                fseight.add(i.fsrecipe)
    left = profile.diet.day_target_calories - consumed
    rate = (consumed * 100) / profile.diet.day_target_calories
    water_rate = (water_consumed * 100) / profile.diet.water
    protein_rate = (protein_consumed * 100) / profile.diet.proteins
    carbo_rate = (carbo_consumed * 100) / profile.diet.carbohydrates
    fat_rate = (fat_consumed * 100) / profile.diet.fats
    recommend = profile.diet.day_target_calories / meals
    water = Recipe.objects.get(name_en='Water')
    if rate > 100:
        rate = 100
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete_meal = MealsDeleteForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        save = SavedRecipeForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
                return redirect('projectx:profile_diet', profile_slug=profile.slug)
        elif save.is_valid():
            rec = save.cleaned_data['recipes']
            for i in rec:
                profile.recipes.add(i)
                return redirect('projectx:profile_diet', profile_slug=profile.slug)
        elif delete_meal.is_valid():
            meal = Meals.objects.filter(user_id=form.cleaned_data['user'],
                                        created__date=date.today(),
                                        meals_per_day=form.cleaned_data['meals_per_day'],
                                        fsrecipe=form.cleaned_data['fsrecipe'],
                                       )
            for i in meal:
                i.delete()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
        save = SavedRecipeForm()
        delete_meal = MealsDeleteForm()
    context = {'form': form,
               'delete': delete,
               'meals_delete': delete_meal,
               'save': save,
               'p': p,
               'c': c,
               'd': d,
               'dd': dd,
               'profile': profile,
               'meals': meals,
               'recipes': recipe,
               'meals_names': meals_name,
               'meals_finish': meals_finish,
               'consumed': int(consumed),
               'protein_consumed': int(protein_consumed),
               'carbo_consumed': int(carbo_consumed),
               'fat_consumed': int(fat_consumed),
               'protein_rate': int(protein_rate),
               'carbo_rate': int(carbo_rate),
               'fat_rate': int(fat_rate),
               'water_consumed': int(water_consumed),
	           'voda': water,
               'left': int(left),
               'rate': int(rate),
               'one': one,
               'two': two,
               'three': three,
               'four': four,
               'five': five,
               'six': six,
               'seven': seven,
               'eight': eight,
               'fsone': fsone,
               'fstwo': fstwo,
               'fsthree': fsthree,
               'fsfour': fsfour,
               'fsfive': fsfive,
               'fssix': fssix,
               'fsseven': fsseven,
               'fseight': fseight,
               'recommend': int(recommend),
               'friend_request': friend_request,
               'messages': messages,
               'diet': diet,
               'proteins': int(diet.proteins),
               'carbo': int(diet.carbohydrates),
               'fats': int(diet.fats),
               'water': diet.water,
               'w_rate': int(water_rate),
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/profile-diet-2.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-diet.html', context)


def simple_category_recipe(request, profile_slug, meals_slug, category_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    profile = Profile.objects.get(slug=profile_slug)
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')
    category = FSRecipeCategory.objects.get(slug=category_slug)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDayName.objects.get(slug=meals_slug)
    recipes = meals_name.fsrecipe_set.filter(recipe_category_id=category.id)
    recommend = profile.diet.day_target_calories / meals
    if category.id == 1:
        for i in recipes:
            portion = count_proteins(profile, i)
            i.portion = int(portion * 100)
            i.proteins = portion * i.proteins
            i.carbohydrates = portion * i.carbohydrates
            i.fats = portion * i.fats
            i.calories = portion * i.calories
    elif category.id == 2:
        for i in recipes:
            portion = count_carbo(profile, i)
            i.portion = int(portion * 100)
            i.proteins = portion * i.proteins
            i.carbohydrates = portion * i.carbohydrates
            i.fats = portion * i.fats
            i.calories = portion * i.calories
    if request.method == "POST":
        form = MealsForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            recipe = FSRecipe.objects.get(id=meal.fsrecipe_id)
            for i in recipes:
                if i.id == recipe.id:
                    proteins = i.proteins
                    carbo = i.carbohydrates
                    fats = i.fats
                    calories = i.calories
                    portion = i.portion
            if category.id != 1 and category.id != 2:
                proteins = recipe.proteins
                carbo = recipe.carbohydrates
                fats = recipe.fats
                calories = recipe.calories
            meal.proteins = proteins
            meal.carbohydrates = carbo
            meal.fats = fats
            meal.water = recipe.water
            meal.calories = calories
            meal.portion = portion
            meal.save()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
    else:
        form = MealsForm()
    context = {'form': form,
               'profile': profile,
               'recipes': recipes,
               'friend_request': friend_request,
               'category': category,
               'recommend': int(recommend),
               'meal': meals_name,
               'messages': messages,
               }
    return render(request, 'projectx/profile-simple-recipe-category.html', context)


@csrf_exempt
def simple_recipe(request, profile_slug, meals_slug, category_slug, recipe_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    profile = Profile.objects.get(slug=profile_slug)
    recipe = get_object_or_404(FSRecipe, slug=recipe_slug)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDayName.objects.get(slug=meals_slug)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    recommend = profile.diet.day_target_calories / meals
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')

    category = FSRecipeCategory.objects.get(slug=category_slug)
    proteins = recipe.proteins
    carbo = recipe.carbohydrates
    fats = recipe.fats
    calories = recipe.calories
    portion = recipe.portion
    if category.id == 1:
        portion = count_proteins(profile, recipe)
        proteins = portion * recipe.proteins
        carbo = portion * recipe.carbohydrates
        fats = portion * recipe.fats
        calories = portion * recipe.calories
        portion = portion * 100
    elif category.id == 2:
        portion = count_carbo(profile, recipe)
        proteins = portion * recipe.proteins
        carbo = portion * recipe.carbohydrates
        fats = portion * recipe.fats
        calories = portion * recipe.calories
        portion = portion * 100

    if request.method == "POST":
        form = MealsForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = FSRecipe.objects.get(id=meal.fsrecipe_id)
            meal.proteins = proteins
            meal.carbohydrates = carbo
            meal.fats = fats
            meal.water = meal_recipe.water
            meal.calories = calories
            meal.portion = portion
            meal.save()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
    else:
        form = MealsForm()
    context = ({'form': form,
                'profile': profile,
                'recipe': recipe,
                'friend_request': friend_request,
                'messages': messages,
                'meals': meals_name,
                'recommend': int(recommend),
                'proteins': int(proteins),
                'carbo': int(carbo),
                'fats': int(fats),
                'calories': int(calories),
                'portion': int(portion),
                })
    return render(request, 'projectx/recipe.html', context)


@csrf_exempt
def profile_recipe(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    profile_recipe_deleted = profile.delete_recipes.all()
    fs = RecipeCategory.objects.get(id=1)
    garnish = RecipeCategory.objects.get(id=2)
    meat = RecipeCategory.objects.get(id=3)
    salad = RecipeCategory.objects.get(id=4)
    soup = RecipeCategory.objects.get(id=5)
    dessert = RecipeCategory.objects.get(id=6)
    drink = RecipeCategory.objects.get(id=7)
    dirty = RecipeCategory.objects.get(id=8)
    fs_recipe = profile.recipes.all()
    garnish_recipe = Recipe.objects.filter(recipe_category_id=2)
    meat_recipe = Recipe.objects.filter(recipe_category_id=3)
    salad_recipe = Recipe.objects.filter(recipe_category_id=4)
    soup_recipe = Recipe.objects.filter(recipe_category_id=5)
    dessert_recipe = Recipe.objects.filter(recipe_category_id=6)
    drink_recipe = Recipe.objects.filter(recipe_category_id=7)
    dirty_recipe = Recipe.objects.filter(recipe_category_id=8)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    recommend = profile.diet.day_target_calories / meals
    for i in fs_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                fs_recipe = fs_recipe.exclude(id=j.id)
    for i in garnish_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                garnish_recipe = garnish_recipe.exclude(id=j.id)
    for i in meat_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                meat_recipe = meat_recipe.exclude(id=j.id)
    for i in salad_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                salad_recipe = salad_recipe.exclude(id=j.id)
    for i in soup_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                soup_recipe = soup_recipe.exclude(id=j.id)
    for i in dessert_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                dessert_recipe = dessert_recipe.exclude(id=j.id)
    for i in drink_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                drink_recipe = drink_recipe.exclude(id=j.id)
    for i in dirty_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                dirty_recipe = dirty_recipe.exclude(id=j.id)
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
            return redirect('projectx:profile_recipe', profile_slug=profile.slug)
        elif find.is_valid():
            recipe = find.cleaned_data['name']
            request.session['result'] = recipe
            return redirect('projectx:recipe_search', profile_slug=profile.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
        find = FindFriendForm(request.POST)
    context = {'form': form,
               'delete': delete,
               'find': find,
               'profile': profile,
               'fs_recipes': fs_recipe,
               'garnish_recipes': garnish_recipe,
               'meat_recipes': meat_recipe,
               'salad_recipes': salad_recipe,
               'soup_recipes': soup_recipe,
               'dessert_recipes': dessert_recipe,
               'drink_recipes': drink_recipe,
               'dirty_recipes': dirty_recipe,
               'recommend': int(recommend),
               'meals': meals_name,
               'friend_request': friend_request,
               'fs': fs,
               'garnish': garnish,
               'meat': meat,
               'salad': salad,
               'soup': soup,
               'drink': drink,
               'dessert': dessert,
               'dirty': dirty,
               'messages': messages,
               }

    if request.is_ajax():
        rendered = render_to_string('projectx/profile-recipe-2.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-recipe.html', context)


@csrf_exempt
def recipe_search(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    result = request.session.get('result')
    search_results = watson.search(result, models=(Recipe,))
    recipes = set()
    profile_recipe_deleted = profile.delete_recipes.all()
    for item in search_results:
        for i in profile_recipe_deleted:
            if item.title == i.name:
                search_results = search_results.exclude(id=item.id)
    for item in search_results:
        recipes.add(Recipe.objects.get(name=item))
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    recommend = profile.diet.day_target_calories / meals
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
            return redirect('projectx:recipe_search', profile_slug=profile.slug)
        elif find.is_valid():
            recipe = find.cleaned_data['name']
            request.session['result'] = recipe
            return redirect('projectx:recipe_search', profile_slug=profile.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
        find = FindFriendForm(request.POST)
    context = {'form': form,
               'delete': delete,
               'find': find,
               'profile': profile,
               'recipes': recipes,
               'recommend': int(recommend),
               'meals': meals_name,
               'friend_request': friend_request,
               'messages': messages,
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/recipe-search-2.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/recipe-search.html', context)


@csrf_exempt
def category_recipe(request, profile_slug, category_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')
    category = RecipeCategory.objects.get(slug=category_slug)
    if category.id == 1:
        recipes = profile.recipes.all()
    else:
        recipes = Recipe.objects.filter(recipe_category_id=category.id)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    recommend = profile.diet.day_target_calories / meals
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:category_recipe', profile_slug=profile.slug, category_slug=category.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
            return redirect('projectx:category_recipe', profile_slug=profile.slug, category_slug=category.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
    context = {'delete': delete,
               'form': form,
               'profile': profile,
               'recipes': recipes,
               'friend_request': friend_request,
               'category': category,
               'recommend': int(recommend),
               'meals': meals_name,
               'messages': messages,
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/profile-recipe-category-2.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-recipe-category.html', context)


def quick_decision(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')

    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    daytime = DayTime.objects.all()
    sources = Sources.objects.all()
    if request.method == "POST":
        form = VariantForm(request.POST)
        if form.is_valid():
            day = form.cleaned_data['day_time']
            source = form.cleaned_data['source']
            target = form.cleaned_data['target']
            variant = Variants.objects.filter(source_id=source, target_id=target, day_time_id=day)
            v = Var()
            v.save()
            for var in variant:
                v.variants.add(var.id)
            v.save()
            return redirect('projectx:quick_decision_done', profile_slug=profile.slug, var=v.id)
    else:
        form = VariantForm()
    return render(request, 'projectx/profile-quick-decision.html', {'profile': profile,
                                                                    'form': form,
                                                                    'daytime': daytime,
                                                                    'sources': sources,
                                                                    'friend_request': friend_request,
                                                                    'messages': messages,
                                                                    })


def quick_decision_done(request, profile_slug, var):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    variatns = Var.objects.get(id=var)
    var = variatns.variants.all()
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    return render(request, 'projectx/profile-quick-decision-done.html', {'profile': profile,
                                                                         'variants': var,
                                                                         'friend_request': friend_request,
                                                                         'messages': messages,
                                                                         })


@csrf_exempt
def profile_friends(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend = profile.friends.all().order_by('id')
    friend_request = profile.friend_request.all().order_by('id')
    page = request.GET.get('page2')
    paginator_friends = Paginator(friend, 27)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    try:
        friends = paginator_friends.page(page)
    except PageNotAnInteger:
        friends = paginator_friends.page(1)
    except EmptyPage:
        friends = paginator_friends.page(paginator_friends.num_pages)
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        remove = RemoveFriendForm(request.POST)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            name = form.cleaned_data['name']
            if name == 'add':
                for i in friend:
                    profile.friends.add(i.id)
                    profile.friend_request.remove(i.id)
                    i.profile.friends.add(profile.user_id)
                    i.save()
            elif name == 'delete':
                for i in friend:
                    profile.friends.remove(i.id)
                    i.friends.remove(profile.user_id)
                    i.save()
            profile.save()
            return redirect('projectx:profile_friends', profile_slug=profile.slug)
        elif remove.is_valid():
            friend = remove.cleaned_data['friend_request']
            for i in friend:
                profile.friend_request.remove(i.id)
            profile.save()
            return redirect('projectx:profile_friends', profile_slug=profile.slug)
        elif find.is_valid():
            friend = find.cleaned_data['name']
            request.session['result'] = friend
            return redirect('projectx:friend_search', profile_slug=profile.slug)
    else:
        form = AddFriendForm()
        remove = RemoveFriendForm(request.POST)
        find = FindFriendForm(request.POST)
    context = {'profile': profile,
               'form': form,
               'find': find,
               'friends': friends,
               'friends_request': friend_request,
               'friend_request': friend_request,
               'remove': remove,
               'messages': messages,
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/profile-friends-2.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-friends.html', context)


def friend_search(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    result = request.session.get('result')
    search_results = watson.search(result, models=(Profile,))
    friend = set()
    for item in search_results:
        friend.add(Profile.objects.get(name_slug=item))
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            for i in friend:
                i.profile.friend_request.add(profile.user.id)
                i.save()
            return redirect('projectx:profile_friends', profile_slug=profile.slug)
        elif find.is_valid():
            friend = find.cleaned_data['name']
            request.session['result'] = friend
            return redirect('projectx:friend_search', profile_slug=profile.slug)
    else:
        form = AddFriendForm()
        find = FindFriendForm()
    friend_request = profile.friend_request.all().order_by('id')
    context = {'profile': profile,
               'form': form,
	       'find': find,
               'friends': friend,
               'messages': messages,
               'friend_request': friend_request,
               }
    return render(request, 'projectx/friend-search.html', context)


def profile_user(request, profile_slug, user_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    user_ = get_object_or_404(Profile, slug=user_slug)
    friend = user_.friends.all().order_by('id')
    own_friend = profile.friends.all().order_by('id')
    common_friends = set()
    if_friend = False
    for i in own_friend:
        if i == user_.user:
            if_friend = True
    for f in friend:
        for o in own_friend:
            if f == o:
                common_friends.add(f)
                friend = friend.exclude(id=f.id)
    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    status = False
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    page = request.GET.get('page')
    paginator_friends = Paginator(friend, 6)
    try:
        friends = paginator_friends.page(page)
    except PageNotAnInteger:
        friends = paginator_friends.page(1)
    except EmptyPage:
        friends = paginator_friends.page(paginator_friends.num_pages)
    for userv in user_status:
        if userv.user == user_.user:
            status = True
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            for i in friend:
                i.profile.friend_request.add(profile.user.id)
                i.save()
            return redirect('projectx:profile_user', profile_slug=profile.slug, user_slug=user_.slug)
    else:
        form = AddFriendForm()
    return render(request, 'projectx/profile-user.html', {'profile': profile,
                                                          'form': form,
                                                          'user_': user_,
                                                          'status': status,
                                                          'friends': friends,
                                                          'common_friends': common_friends,
                                                          'if_friend': if_friend,
                                                          'friend_request': friend_request,
                                                          'messages': messages,
                                                          })


class HelpView(BaseView):
    template_name = 'projectx/help.html'


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('/form')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def infoform(request):
    user = request.user
    profile = Profile.objects.get(user_id=user.id)
    if profile.info_form == True:
        return redirect('projectx:profile', profile_slug=profile.slug)
    sex = Sex.objects.all()
    type = BodyType.objects.all()
    style = LifeStyle.objects.all()
    target = Target.objects.all()
    if request.method == "POST":
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            profile.info_form = True
            profile.change_time = False
            profile.last_update = datetime.datetime.now()
            profile.save()
            return redirect('projectx:profile', profile_slug=profile.slug)
    else:
        form = ProfileForm()
    return render(request, 'projectx/info-form.html', {'form': form, 'sex': sex, 'type': type, 'target': target,
                                                       'style': style, })


def profile_setting(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    targets = Target.objects.all()
    recipes = profile.delete_recipes.all()
    for i in targets:
        if i.id == profile.target_id:
            targets = targets.exclude(id=i.id)
    styles = LifeStyle.objects.all()
    for i in styles:
        if i.id == profile.life_style_id:
            styles = styles.exclude(id=i.id)
    if request.method == "POST":
        photo = PhotoForm(request.POST, request.FILES, instance=profile)
        main = SettingMainForm(request.POST)
        delete = DeleteRecipeForm(request.POST)
        detail = AboutForm(request.POST)
        if photo.is_valid():
            photo.save()
            return redirect('projectx:profile', profile_slug=profile.slug)
        elif delete.is_valid():
            recipe = delete.cleaned_data['delete_recipes']
            for i in recipe:
                profile.delete_recipes.remove(i)
            return redirect('projectx:profile_setting', profile_slug=profile.slug)
        elif main.is_valid():
            style = main.cleaned_data['life_style']
            target = main.cleaned_data['target']
            weight = main.cleaned_data['weight']
            if style == None:
                pass
            else:
                p_weight = profile.weight
                profile.life_style = LifeStyle.objects.get(name=style)
                profile.target = Target.objects.get(name=target)
                profile.weight = weight
                profile.change_time = False
                profile.last_update = datetime.datetime.now()
                if p_weight == weight:
                    diet = Diet.objects.get(id=profile.diet_id)
                    if profile.target_id == 1:
                        diet.adaptation = diet.adaptation - 100
                    elif profile.target_id == 2:
                        diet.adaptation = diet.adaptation + 100
                    diet.save()
                profile.save()
                return redirect('projectx:profile_setting', profile_slug=profile.slug)
        if detail.is_valid():
            status = detail.cleaned_data['status']
            facebook = detail.cleaned_data['facebook']
            instagram = detail.cleaned_data['instagram']
            country = detail.cleaned_data['country']
            city = detail.cleaned_data['city']
            if status != None:
                profile.status = status
            if facebook != None:
                profile.facebook = facebook
            if instagram != None:
                profile.instagram = instagram
            if country != None:
                profile.country = country
            if city != None:
                profile.city = city
            profile.save()
            bonus = detail.cleaned_data['bonus']
            if bonus:
                profile.bonus = bonus.lower()
                profile.save()
                return redirect('projectx:profile_setting', profile_slug=profile.slug)
            else:
                return redirect('projectx:profile', profile_slug=profile.slug)
    else:
        photo = PhotoForm()
        main = SettingMainForm()
        delete = DeleteRecipeForm()
        detail = AboutForm()
    bonus = Bonus.objects.all()
    cod = False
    discount = 0
    for i in bonus:
        if i.value == profile.bonus:
            discount = i.discount
            cod = True
    price = 8 - discount
    liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
    if profile.order_id == None:
        profile.order_id = profile.user.username
        profile.save()
    params = {
        'action': 'subscribe',
        'amount': price,
        'currency': 'USD',
        'description': 'First Step підписка',
        'order_id': profile.order_id,
        'version': '3',
        'subscribe_periodicity': 'month',
        'subscribe_date_start': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
        'server_url': 'https://first-step-diet.com/pay-callback/',
        'language': 'ua',
    }
    cancel = {
        'action': 'unsubscribe',
        'version': '3',
        'order_id': profile.order_id,
    }
    status = 1
    if profile.subscription_ends != None:
        if profile.subscription_ends > timezone.now() and profile.subscription == False:
            status = 2
        if profile.subscription_ends > timezone.now() and profile.subscription == True:
            status = 3
    signature = liqpay.cnb_signature(params)
    data = liqpay.cnb_data(params)
    cancel_signature = liqpay.cnb_signature(cancel)
    cancel_data = liqpay.cnb_data(cancel)
    return render(request, 'projectx/profile-settings.html', {'photo': photo,
                                                              'main': main,
                                                              'delete': delete,
							      'detail': detail,
                                                              'profile': profile,
                                                              'friend_request': friend_request,
                                                              'messages': messages,
                                                              'styles': styles,
                                                              'targets': targets,
                                                              'recipes': recipes,
                                                              'signature': signature,
                                                              'data': data,
                                                              'cancel_data': cancel_data,
                                                              'cancel_signature': cancel_signature,
                                                              'bonus': cod,
                                                              'status': status,
                                                              'price': price,
                                                              'discount': discount,
                                                              })


class IndexRuView(BaseView):
    template_name = 'projectx/index_ru.html'


class AboutRuView(BaseView):
    template_name = 'projectx/about-ru.html'


class ContactRuView(BaseView):
    template_name = 'projectx/contact-ru.html'


def profileru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    article = Article.objects.filter(article_of_day=True)
    for i in article:
        article = i
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    status = False
    friend = Profile.objects.order_by("?")[:3]
    for userv in user_status:
        if userv.user == profile.user:
            status = True

    history = profile.history.all()
    weight = []
    time = []
    for i in history:
        if i.prev_record:
            if i.prev_record.weight != i.weight:
                weight.append(i.weight)
                time.append(i.history_date)
    weight.reverse()
    time.reverse()
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            for i in friend:
                i.profile.friend_request.add(profile.user_id)
                i.save()
            profile.save()
            return redirect('projectx:profileru', profile_slug=profile.slug)
    else:
        form = AddFriendForm()
    return render(request, 'projectx/profile-main-ru.html', {'profile': profile,
                                                             'form': form,
                                                             'friend_request': friend_request,
                                                             'messages': messages,
                                                             'status': status,
                                                             'article': article,
                                                             'friends': friend,
                                                             'weight': weight,
                                                             'time': time,
                                                             'created': profile.created,
                                                             })


@csrf_exempt
def profile_dietru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    diet = Diet.objects.get(id=profile.diet.id)
    friend_request = profile.friend_request.all().order_by('id')
    recipe = Recipe.objects.filter(recipe_category_id=1)
    profile_recipe_deleted = profile.delete_recipes.all()
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all().order_by('id')
    meals_finish = Meals.objects.filter(user_id=user.id, created__date=date.today())
    consumed = 0
    protein_consumed = 0
    carbo_consumed = 0
    fat_consumed = 0
    water_consumed = 0
    one = set()
    two = set()
    three = set()
    four = set()
    five = set()
    six = set()
    seven = set()
    eight = set()
    fsone = set()
    fstwo = set()
    fsthree = set()
    fsfour = set()
    fsfive = set()
    fssix = set()
    fsseven = set()
    fseight = set()
    p = FSRecipeCategory.objects.get(id=1)
    c = FSRecipeCategory.objects.get(id=2)
    d = FSRecipeCategory.objects.get(id=3)
    dd = FSRecipeCategory.objects.get(id=4)
    for i in recipe:
        for j in profile_recipe_deleted:
            if i == j:
                recipe = recipe.exclude(id=j.id)
    for i in meals_finish:
        consumed += i.calories
        protein_consumed += i.proteins
        carbo_consumed += i.carbohydrates
        fat_consumed += i.fats
        water_consumed += i.water
        if i.recipe and i.recipe.name_en != ('Water'):
            if i.meals_per_day_id == 1:
                one.add(i.recipe)
            elif i.meals_per_day_id == 2:
                two.add(i.recipe)
            elif i.meals_per_day_id == 3:
                three.add(i.recipe)
            elif i.meals_per_day_id == 4:
                four.add(i.recipe)
            elif i.meals_per_day_id == 5:
                five.add(i.recipe)
            elif i.meals_per_day_id == 6:
                six.add(i.recipe)
            elif i.meals_per_day_id == 7:
                seven.add(i.recipe)
            elif i.meals_per_day_id == 8:
                eight.add(i.recipe)
        if i.fsrecipe:
            if i.meals_per_day_id == 1:
                fsone.add(i.fsrecipe)
            elif i.meals_per_day_id == 2:
                fstwo.add(i.fsrecipe)
            elif i.meals_per_day_id == 3:
                fsthree.add(i.fsrecipe)
            elif i.meals_per_day_id == 4:
                fsfour.add(i.fsrecipe)
            elif i.meals_per_day_id == 5:
                fsfive.add(i.fsrecipe)
            elif i.meals_per_day_id == 6:
                fssix.add(i.fsrecipe)
            elif i.meals_per_day_id == 7:
                fsseven.add(i.fsrecipe)
            elif i.meals_per_day_id == 8:
                fseight.add(i.fsrecipe)
    left = profile.diet.day_target_calories - consumed
    rate = (consumed * 100) / profile.diet.day_target_calories
    water_rate = (water_consumed * 100) / profile.diet.water
    protein_rate = (protein_consumed * 100) / profile.diet.proteins
    carbo_rate = (carbo_consumed * 100) / profile.diet.carbohydrates
    fat_rate = (fat_consumed * 100) / profile.diet.fats
    recommend = profile.diet.day_target_calories / meals
    water = Recipe.objects.get(name_en='Water')
    if rate > 100:
        rate = 100
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete_meal = MealsDeleteForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        save = SavedRecipeForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
                return redirect('projectx:profile_dietru', profile_slug=profile.slug)
        elif save.is_valid():
            rec = save.cleaned_data['recipes']
            for i in rec:
                profile.recipes.add(i)
                return redirect('projectx:profile_dietru', profile_slug=profile.slug)
        elif delete_meal.is_valid():
            meal = Meals.objects.filter(user_id=form.cleaned_data['user'],
                                        created__date=date.today(),
                                        meals_per_day=form.cleaned_data['meals_per_day'],
                                        fsrecipe=form.cleaned_data['fsrecipe'],
                                       )
            for i in meal:
                i.delete()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
        save = SavedRecipeForm()
        delete_meal = MealsDeleteForm()
    context = {'form': form,
               'delete': delete,
               'meals_delete': delete_meal,
               'save': save,
               'p': p,
               'c': c,
               'd': d,
               'dd': dd,
               'profile': profile,
               'meals': meals,
               'recipes': recipe,
               'meals_names': meals_name,
               'meals_finish': meals_finish,
               'consumed': int(consumed),
               'protein_consumed': int(protein_consumed),
               'carbo_consumed': int(carbo_consumed),
               'fat_consumed': int(fat_consumed),
               'protein_rate': int(protein_rate),
               'carbo_rate': int(carbo_rate),
               'fat_rate': int(fat_rate),
               'water_consumed': int(water_consumed),
	           'voda': water,
               'left': int(left),
               'rate': int(rate),
               'one': one,
               'two': two,
               'three': three,
               'four': four,
               'five': five,
               'six': six,
               'seven': seven,
               'eight': eight,
               'fsone': fsone,
               'fstwo': fstwo,
               'fsthree': fsthree,
               'fsfour': fsfour,
               'fsfive': fsfive,
               'fssix': fssix,
               'fsseven': fsseven,
               'fseight': fseight,
               'recommend': int(recommend),
               'friend_request': friend_request,
               'messages': messages,
               'diet': diet,
               'proteins': int(diet.proteins),
               'carbo': int(diet.carbohydrates),
               'fats': int(diet.fats),
               'water': diet.water,
               'w_rate': int(water_rate),
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/profile-diet-2-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-diet-ru.html', context)


def simple_category_reciperu(request, profile_slug, meals_slug, category_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    profile = Profile.objects.get(slug=profile_slug)
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')
    category = FSRecipeCategory.objects.get(slug=category_slug)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDayName.objects.get(slug=meals_slug)
    recipes = meals_name.fsrecipe_set.filter(recipe_category_id=category.id)
    recommend = profile.diet.day_target_calories / meals
    if category.id == 1:
        for i in recipes:
            portion = count_proteins(profile, i)
            i.portion = int(portion * 100)
            i.proteins = portion * i.proteins
            i.carbohydrates = portion * i.carbohydrates
            i.fats = portion * i.fats
            i.calories = portion * i.calories
    elif category.id == 2:
        for i in recipes:
            portion = count_carbo(profile, i)
            i.portion = int(portion * 100)
            i.proteins = portion * i.proteins
            i.carbohydrates = portion * i.carbohydrates
            i.fats = portion * i.fats
            i.calories = portion * i.calories
    if request.method == "POST":
        form = MealsForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            recipe = FSRecipe.objects.get(id=meal.fsrecipe_id)
            for i in recipes:
                if i.id == recipe.id:
                    proteins = i.proteins
                    carbo = i.carbohydrates
                    fats = i.fats
                    calories = i.calories
                    portion = i.portion
            if category.id != 1 and category.id != 2:
                proteins = recipe.proteins
                carbo = recipe.carbohydrates
                fats = recipe.fats
                calories = recipe.calories
            meal.proteins = proteins
            meal.carbohydrates = carbo
            meal.fats = fats
            meal.water = recipe.water
            meal.calories = calories
            meal.portion = portion
            meal.save()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
    else:
        form = MealsForm()
    context = {'form': form,
               'profile': profile,
               'recipes': recipes,
               'friend_request': friend_request,
               'category': category,
               'recommend': int(recommend),
               'meal': meals_name,
               'messages': messages,
               }
    return render(request, 'projectx/profile-simple-recipe-category-ru.html', context)


@csrf_exempt
def simple_reciperu(request, profile_slug, meals_slug, category_slug, recipe_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    profile = Profile.objects.get(slug=profile_slug)
    recipe = get_object_or_404(FSRecipe, slug=recipe_slug)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDayName.objects.get(slug=meals_slug)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    recommend = profile.diet.day_target_calories / meals
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')

    category = FSRecipeCategory.objects.get(slug=category_slug)
    proteins = recipe.proteins
    carbo = recipe.carbohydrates
    fats = recipe.fats
    calories = recipe.calories
    portion = recipe.portion
    if category.id == 1:
        portion = count_proteins(profile, recipe)
        proteins = portion * recipe.proteins
        carbo = portion * recipe.carbohydrates
        fats = portion * recipe.fats
        calories = portion * recipe.calories
        portion = portion * 100
    elif category.id == 2:
        portion = count_carbo(profile, recipe)
        proteins = portion * recipe.proteins
        carbo = portion * recipe.carbohydrates
        fats = portion * recipe.fats
        calories = portion * recipe.calories
        portion = portion * 100

    if request.method == "POST":
        form = MealsForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = FSRecipe.objects.get(id=meal.fsrecipe_id)
            meal.proteins = proteins
            meal.carbohydrates = carbo
            meal.fats = fats
            meal.water = meal_recipe.water
            meal.calories = calories
            meal.portion = portion
            meal.save()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
    else:
        form = MealsForm()
    context = ({'form': form,
                'profile': profile,
                'recipe': recipe,
                'friend_request': friend_request,
                'messages': messages,
                'meals': meals_name,
                'recommend': int(recommend),
                'proteins': int(proteins),
                'carbo': int(carbo),
                'fats': int(fats),
                'calories': int(calories),
                'portion': int(portion),
                })
    return render(request, 'projectx/recipe-ru.html', context)


@csrf_exempt
def profile_reciperu(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    profile_recipe_deleted = profile.delete_recipes.all()
    fs = RecipeCategory.objects.get(id=1)
    garnish = RecipeCategory.objects.get(id=2)
    meat = RecipeCategory.objects.get(id=3)
    salad = RecipeCategory.objects.get(id=4)
    soup = RecipeCategory.objects.get(id=5)
    dessert = RecipeCategory.objects.get(id=6)
    drink = RecipeCategory.objects.get(id=7)
    dirty = RecipeCategory.objects.get(id=8)
    fs_recipe = profile.recipes.all()
    garnish_recipe = Recipe.objects.filter(recipe_category_id=2)
    meat_recipe = Recipe.objects.filter(recipe_category_id=3)
    salad_recipe = Recipe.objects.filter(recipe_category_id=4)
    soup_recipe = Recipe.objects.filter(recipe_category_id=5)
    dessert_recipe = Recipe.objects.filter(recipe_category_id=6)
    drink_recipe = Recipe.objects.filter(recipe_category_id=7)
    dirty_recipe = Recipe.objects.filter(recipe_category_id=8)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    recommend = profile.diet.day_target_calories / meals
    for i in fs_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                fs_recipe = fs_recipe.exclude(id=j.id)
    for i in garnish_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                garnish_recipe = garnish_recipe.exclude(id=j.id)
    for i in meat_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                meat_recipe = meat_recipe.exclude(id=j.id)
    for i in salad_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                salad_recipe = salad_recipe.exclude(id=j.id)
    for i in soup_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                soup_recipe = soup_recipe.exclude(id=j.id)
    for i in dessert_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                dessert_recipe = dessert_recipe.exclude(id=j.id)
    for i in drink_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                drink_recipe = drink_recipe.exclude(id=j.id)
    for i in dirty_recipe:
        for j in profile_recipe_deleted:
            if i == j:
                dirty_recipe = dirty_recipe.exclude(id=j.id)
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
            return redirect('projectx:profile_reciperu', profile_slug=profile.slug)
        elif find.is_valid():
            recipe = find.cleaned_data['name']
            request.session['result'] = recipe
            return redirect('projectx:recipe_searchru', profile_slug=profile.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
        find = FindFriendForm(request.POST)
    context = {'form': form,
               'delete': delete,
               'find': find,
               'profile': profile,
               'fs_recipes': fs_recipe,
               'garnish_recipes': garnish_recipe,
               'meat_recipes': meat_recipe,
               'salad_recipes': salad_recipe,
               'soup_recipes': soup_recipe,
               'dessert_recipes': dessert_recipe,
               'drink_recipes': drink_recipe,
               'dirty_recipes': dirty_recipe,
               'recommend': int(recommend),
               'meals': meals_name,
               'friend_request': friend_request,
               'fs': fs,
               'garnish': garnish,
               'meat': meat,
               'salad': salad,
               'soup': soup,
               'drink': drink,
               'dessert': dessert,
               'dirty': dirty,
               'messages': messages,
               }

    if request.is_ajax():
        rendered = render_to_string('projectx/profile-recipe-2-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-recipe-ru.html', context)


@csrf_exempt
def recipe_searchru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    result = request.session.get('result')
    search_results = watson.search(result, models=(Recipe,))
    recipes = set()
    profile_recipe_deleted = profile.delete_recipes.all()
    for item in search_results:
        for i in profile_recipe_deleted:
            if item.title == i.name:
                search_results = search_results.exclude(id=item.id)
    for item in search_results:
        recipes.add(Recipe.objects.get(name=item))
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    recommend = profile.diet.day_target_calories / meals
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
            return redirect('projectx:recipe_searchru', profile_slug=profile.slug)
        elif find.is_valid():
            recipe = find.cleaned_data['name']
            request.session['result'] = recipe
            return redirect('projectx:recipe_searchru', profile_slug=profile.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
        find = FindFriendForm(request.POST)
    context = {'form': form,
               'delete': delete,
               'find': find,
               'profile': profile,
               'recipes': recipes,
               'recommend': int(recommend),
               'meals': meals_name,
               'friend_request': friend_request,
               'messages': messages,
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/recipe-search-2-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/recipe-search-ru.html', context)


@csrf_exempt
def category_reciperu(request, profile_slug, category_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')
    category = RecipeCategory.objects.get(slug=category_slug)
    if category.id == 1:
        recipes = profile.recipes.all()
    else:
        recipes = Recipe.objects.filter(recipe_category_id=category.id)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    recommend = profile.diet.day_target_calories / meals
    if request.method == "POST":
        form = MealsForm(request.POST)
        delete = DeleteRecipeForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:category_reciperu', profile_slug=profile.slug, category_slug=category.slug)
        elif delete.is_valid():
            rec = delete.cleaned_data['delete_recipes']
            for i in rec:
                profile.delete_recipes.add(i)
                profile.recipes.remove(i)
            return redirect('projectx:category_reciperu', profile_slug=profile.slug, category_slug=category.slug)
    else:
        form = MealsForm()
        delete = DeleteRecipeForm()
    context = {'delete': delete,
               'form': form,
               'profile': profile,
               'recipes': recipes,
               'friend_request': friend_request,
               'category': category,
               'recommend': int(recommend),
               'meals': meals_name,
               'messages': messages,
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/profile-recipe-category-2-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-recipe-category-ru.html', context)


def quick_decisionru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')

    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    daytime = DayTime.objects.all()
    sources = Sources.objects.all()
    if request.method == "POST":
        form = VariantForm(request.POST)
        if form.is_valid():
            day = form.cleaned_data['day_time']
            source = form.cleaned_data['source']
            target = form.cleaned_data['target']
            variant = Variants.objects.filter(source_id=source, target_id=target, day_time_id=day)
            v = Var()
            v.save()
            for var in variant:
                v.variants.add(var.id)
            v.save()
            return redirect('projectx:quick_decision_doneru', profile_slug=profile.slug, var=v.id)
    else:
        form = VariantForm()
    return render(request, 'projectx/profile-quick-decision-ru.html', {'profile': profile,
                                                                       'form': form,
                                                                       'daytime': daytime,
                                                                       'sources': sources,
                                                                       'friend_request': friend_request,
                                                                       'messages': messages,
                                                                       })


def quick_decision_doneru(request, profile_slug, var):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    variatns = Var.objects.get(id=var)
    var = variatns.variants.all()
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    return render(request, 'projectx/profile-quick-decision-done-ru.html', {'profile': profile,
                                                                         'variants': var,
                                                                         'friend_request': friend_request,
                                                                         'messages': messages,
                                                                         })


@csrf_exempt
def profile_friendsru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend = profile.friends.all().order_by('id')
    friend_request = profile.friend_request.all().order_by('id')
    page = request.GET.get('page2')
    paginator_friends = Paginator(friend, 27)
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    try:
        friends = paginator_friends.page(page)
    except PageNotAnInteger:
        friends = paginator_friends.page(1)
    except EmptyPage:
        friends = paginator_friends.page(paginator_friends.num_pages)
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        remove = RemoveFriendForm(request.POST)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            name = form.cleaned_data['name']
            if name == 'add':
                for i in friend:
                    profile.friends.add(i.id)
                    profile.friend_request.remove(i.id)
                    i.profile.friends.add(profile.user_id)
                    i.save()
            elif name == 'delete':
                for i in friend:
                    profile.friends.remove(i.id)
                    i.friends.remove(profile.user_id)
                    i.save()
            profile.save()
            return redirect('projectx:profile_friendsru', profile_slug=profile.slug)
        elif remove.is_valid():
            friend = remove.cleaned_data['friend_request']
            for i in friend:
                profile.friend_request.remove(i.id)
            profile.save()
            return redirect('projectx:profile_friendsru', profile_slug=profile.slug)
        elif find.is_valid():
            friend = find.cleaned_data['name']
            request.session['result'] = friend
            return redirect('projectx:friend_searchru', profile_slug=profile.slug)
    else:
        form = AddFriendForm()
        remove = RemoveFriendForm(request.POST)
        find = FindFriendForm(request.POST)
    context = {'profile': profile,
               'form': form,
               'find': find,
               'friends': friends,
               'friends_request': friend_request,
               'friend_request': friend_request,
               'remove': remove,
               'messages': messages,
               }
    if request.is_ajax():
        rendered = render_to_string('projectx/profile-friends-2-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'projectx/profile-friends-ru.html', context)


def friend_searchru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    result = request.session.get('result')
    search_results = watson.search(result, models=(Profile,))
    friend = set()
    for item in search_results:
        friend.add(Profile.objects.get(name_slug=item))
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        find = FindFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            for i in friend:
                i.profile.friend_request.add(profile.user.id)
                i.save()
            return redirect('projectx:profile_friendsru', profile_slug=profile.slug)
        elif find.is_valid():
            friend = find.cleaned_data['name']
            request.session['result'] = friend
            return redirect('projectx:friend_searchru', profile_slug=profile.slug)
    else:
        form = AddFriendForm()
        find = FindFriendForm()
    friend_request = profile.friend_request.all().order_by('id')
    context = {'profile': profile,
               'form': form,
               'find': find,
               'friends': friend,
               'messages': messages,
               'friend_request': friend_request,
               }
    return render(request, 'projectx/friend-search-ru.html', context)


def profile_userru(request, profile_slug, user_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/ru/login')
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    user_ = get_object_or_404(Profile, slug=user_slug)
    friend = user_.friends.all().order_by('id')
    own_friend = profile.friends.all().order_by('id')
    common_friends = set()
    if_friend = False
    for i in own_friend:
        if i == user_.user:
            if_friend = True
    for f in friend:
        for o in own_friend:
            if f == o:
                common_friends.add(f)
                friend = friend.exclude(id=f.id)
    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    status = False
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    page = request.GET.get('page')
    paginator_friends = Paginator(friend, 6)
    try:
        friends = paginator_friends.page(page)
    except PageNotAnInteger:
        friends = paginator_friends.page(1)
    except EmptyPage:
        friends = paginator_friends.page(paginator_friends.num_pages)
    for userv in user_status:
        if userv.user == user_.user:
            status = True
    if request.method == "POST":
        form = AddFriendForm(request.POST)
        if form.is_valid():
            friend = form.cleaned_data['friends']
            for i in friend:
                i.profile.friend_request.add(profile.user.id)
                i.save()
            return redirect('projectx:profile_userru', profile_slug=profile.slug, user_slug=user_.slug)
    else:
        form = AddFriendForm()
    return render(request, 'projectx/profile-user-ru.html', {'profile': profile,
                                                          'form': form,
                                                          'user_': user_,
                                                          'status': status,
                                                          'friends': friends,
                                                          'common_friends': common_friends,
                                                          'if_friend': if_friend,
                                                          'friend_request': friend_request,
                                                          'messages': messages,
                                                          })


class HelpRuView(BaseView):
    template_name = 'projectx/help-ru.html'


def signupru(request):
    if request.method == 'POST':
        form = SignUpFormRu(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('/ru/form')
    else:
        form = SignUpFormRu()
    return render(request, 'signup_ru.html', {'form': form})


def infoformru(request):
    user = request.user
    profile = Profile.objects.get(user_id=user.id)
    if profile.info_form == True:
        return redirect('projectx:profileru', profile_slug=profile.slug)
    sex = Sex.objects.all()
    type = BodyType.objects.all()
    style = LifeStyle.objects.all()
    target = Target.objects.all()
    if request.method == "POST":
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            profile.info_form = True
            profile.change_time = False
            profile.last_update = datetime.datetime.now()
            profile.save()
            return redirect('projectx:profileru', profile_slug=profile.slug)
    else:
        form = ProfileForm()
    return render(request, 'projectx/info-form-ru.html', {'form': form, 'sex': sex, 'type': type, 'target': target,
                                                          'style': style, })


def profile_settingru(request, profile_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    for d in dialogs_2:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    if profile_slug != user.profile.slug:
        profile = get_object_or_404(Profile, user_id=user.id, slug=user.profile.slug)
    else:
        profile = get_object_or_404(Profile, user_id=user.id, slug=profile_slug)
    friend_request = profile.friend_request.all().order_by('id')
    targets = Target.objects.all()
    recipes = profile.delete_recipes.all()
    for i in targets:
        if i.id == profile.target_id:
            targets = targets.exclude(id=i.id)
    styles = LifeStyle.objects.all()
    for i in styles:
        if i.id == profile.life_style_id:
            styles = styles.exclude(id=i.id)
    if request.method == "POST":
        photo = PhotoForm(request.POST, request.FILES, instance=profile)
        main = SettingMainForm(request.POST)
        delete = DeleteRecipeForm(request.POST)
        detail = AboutForm(request.POST)
        if photo.is_valid():
            photo.save()
            return redirect('projectx:profileru', profile_slug=profile.slug)
        elif delete.is_valid():
            recipe = delete.cleaned_data['delete_recipes']
            for i in recipe:
                profile.delete_recipes.remove(i)
            return redirect('projectx:profile_settingru', profile_slug=profile.slug)
        elif main.is_valid():
            style = main.cleaned_data['life_style']
            target = main.cleaned_data['target']
            weight = main.cleaned_data['weight']
            if style == None:
                pass
            else:
                p_weight = profile.weight
                profile.life_style = LifeStyle.objects.get(name=style)
                profile.target = Target.objects.get(name=target)
                profile.weight = weight
                profile.change_time = False
                profile.last_update = datetime.datetime.now()
                if p_weight == weight:
                    diet = Diet.objects.get(id=profile.diet_id)
                    if profile.target_id == 1:
                        diet.adaptation = diet.adaptation - 100
                    elif profile.target_id == 2:
                        diet.adaptation = diet.adaptation + 100
                    diet.save()
                profile.save()
                return redirect('projectx:profile_settingru', profile_slug=profile.slug)
        if detail.is_valid():
            status = detail.cleaned_data['status']
            facebook = detail.cleaned_data['facebook']
            instagram = detail.cleaned_data['instagram']
            country = detail.cleaned_data['country']
            city = detail.cleaned_data['city']
            if status != None:
                profile.status = status
            if facebook != None:
                profile.facebook = facebook
            if instagram != None:
                profile.instagram = instagram
            if country != None:
                profile.country = country
            if city != None:
                profile.city = city
            profile.save()
            bonus = detail.cleaned_data['bonus']
            if bonus:
                profile.bonus = bonus.lower()
                profile.save()
                return redirect('projectx:profile_setting', profile_slug=profile.slug)
            else:
                return redirect('projectx:profileru', profile_slug=profile.slug)
    else:
        photo = PhotoForm()
        main = SettingMainForm()
        delete = DeleteRecipeForm()
        detail = AboutForm()
    bonus = Bonus.objects.all()
    cod = False
    discount = 0
    for i in bonus:
        if i.value == profile.bonus:
            discount = i.discount
            cod = True
    price = 8 - discount
    liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
    if profile.order_id == None:
        profile.order_id = profile.user.username
        profile.save()
    params = {
        'action': 'subscribe',
        'amount': price,
        'currency': 'USD',
        'description': 'First Step підписка',
        'order_id': profile.order_id,
        'version': '3',
        'subscribe_periodicity': 'month',
        'subscribe_date_start': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
        'server_url': 'https://first-step-diet.com/pay-callback/',
        'language': 'ru',
    }
    cancel = {
        'action': 'unsubscribe',
        'version': '3',
        'order_id': profile.order_id,
    }
    signature = liqpay.cnb_signature(params)
    data = liqpay.cnb_data(params)
    cancel_signature = liqpay.cnb_signature(cancel)
    cancel_data = liqpay.cnb_data(cancel)
    status = 1
    if profile.subscription_ends != None:
        if profile.subscription_ends > timezone.now() and profile.subscription == False:
            status = 2
        if profile.subscription_ends > timezone.now() and profile.subscription == True:
            status = 3
    return render(request, 'projectx/profile-settings-ru.html', {'photo': photo,
                                                                 'main': main,
                                                                 'delete': delete,
                                                                 'detail': detail,
                                                                 'profile': profile,
                                                                 'friend_request': friend_request,
                                                                 'messages': messages,
                                                                 'styles': styles,
                                                                 'targets': targets,
                                                                 'recipes': recipes,
                                                                 'signature': signature,
                                                                 'data': data,
                                                                 'cancel_data': cancel_data,
                                                                 'cancel_signature': cancel_signature,
                                                                 'bonus': cod,
                                                                 'status': status,
                                                                 'price': price,
                                                                 'discount': discount,
                                                                 })


@method_decorator(csrf_exempt, name='dispatch')
class PayCallbackView(View):
    def post(self, request, *args, **kwargs):
        # print('hi')
        liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        sign = liqpay.str_to_sign(settings.LIQPAY_PRIVATE_KEY + data + settings.LIQPAY_PRIVATE_KEY)
        if sign == signature:
            print('callback is valid')
        response = liqpay.decode_data_from_str(data)
        # print(response)
        order_id = response['order_id']
        status = response['status']
        profile = Profile.objects.get(order_id=order_id)
        if status == 'subscribed' or status == 'success':
            profile.subscription = True
            profile.subscription_ends = timezone.now() + relativedelta(months=1)
            profile.user_status_id = 2
            profile.save()
        if status == 'unsubscribed':
            profile.subscription = False
            profile.order_id = profile.order_id + '1'
            profile.save()
        return HttpResponse()


def photo_list(request):
    user = request.user
    # profile = Profile.objects.get(user_id=user.id)
    # if user.is_authenticated:
    #     if request.user.profile.info_form == False:
    #         return redirect('/form')
    # else:
    #     return redirect('/ru/login')
    if request.method == "POST":
        print('yes')
        form = ProfileForm(request.POST)
        if form.is_valid():
            print('yesyes')
            print(form)
            # form.save()
            return redirect('/test')
    else:
        form = ProfileForm()
    return render(request, 'projectx/test.html', {
                                                                    'form': form,
                                                                    })

