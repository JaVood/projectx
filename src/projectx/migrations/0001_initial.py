# Generated by Django 2.1.1 on 2019-01-01 19:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('recipe', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BodyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Body Type In Ukraine')),
                ('name_ru', models.CharField(max_length=64, unique=True, verbose_name='Body Type In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='Body Type In English')),
                ('description_en', models.CharField(blank=True, max_length=512, verbose_name='Description In English')),
                ('description_ru', models.CharField(blank=True, max_length=512, verbose_name='Description In Russian')),
                ('description', models.CharField(blank=True, max_length=512, verbose_name='Description In Ukraine')),
            ],
        ),
        migrations.CreateModel(
            name='Bonus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=10, verbose_name='Value')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
        ),
        migrations.CreateModel(
            name='Diet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('boo', models.FloatField(null=True, verbose_name='BOO')),
                ('day_calories_norm', models.FloatField(null=True, verbose_name='Day Calories Norm')),
                ('day_target_calories', models.FloatField(null=True, verbose_name='Day Calories Target')),
                ('proteins', models.FloatField(null=True, verbose_name='Proteins')),
                ('carbohydrates', models.FloatField(null=True, verbose_name='Carbohydrates')),
                ('fats', models.FloatField(null=True, verbose_name='Fats')),
                ('water', models.IntegerField(default='2000', null=True, verbose_name='Water')),
                ('meals_per_day', models.IntegerField(null=True, verbose_name='Meals per day')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HistoricalProfile',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, editable=False, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(blank=True, editable=False, null=True, verbose_name='Updated at')),
                ('last_update', models.DateField(blank=True, null=True, verbose_name='Last Update')),
                ('change_time', models.BooleanField(default=False, verbose_name='Change Time')),
                ('team_member', models.BooleanField(default=False)),
                ('team_image', models.TextField(blank=True, max_length=100, null=True, verbose_name='Team Photo')),
                ('referral_code', models.CharField(blank=True, max_length=10, null=True, verbose_name='Referral Cope')),
                ('phone', models.CharField(blank=True, max_length=100, null=True, verbose_name='Phone')),
                ('file', models.TextField(blank=True, max_length=100, null=True, verbose_name='Photo')),
                ('info_form', models.BooleanField(default=False)),
                ('status', models.CharField(blank=True, max_length=100, null=True, verbose_name='Status')),
                ('name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Name')),
                ('first_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='First Name')),
                ('last_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Last Name')),
                ('name_slug', models.CharField(blank=True, max_length=150, null=True, verbose_name='Name Slug')),
                ('description_en', models.TextField(blank=True, max_length=800, null=True, verbose_name='Description In English')),
                ('description_ru', models.TextField(blank=True, max_length=800, null=True, verbose_name='Description In Russian')),
                ('description', models.TextField(blank=True, max_length=800, null=True, verbose_name='Description In Ukraine')),
                ('facebook', models.URLField(blank=True, null=True, verbose_name='Facebook')),
                ('instagram', models.URLField(blank=True, null=True, verbose_name='Instagram')),
                ('you_tube', models.URLField(blank=True, null=True, verbose_name='You_Tube')),
                ('slug', models.CharField(blank=True, help_text="Don't touch it!", max_length=150, verbose_name='Slug')),
                ('age', models.IntegerField(blank=True, null=True, verbose_name='Age')),
                ('height', models.IntegerField(blank=True, null=True, verbose_name='Height')),
                ('start_weight', models.IntegerField(blank=True, null=True, verbose_name='Start Weight')),
                ('weight', models.FloatField(blank=True, null=True, verbose_name='Weight')),
                ('subscription', models.BooleanField(default=False, verbose_name='Subscription')),
                ('bonus', models.CharField(blank=True, max_length=10, null=True, verbose_name='Bonus')),
                ('subscription_ends', models.DateTimeField(blank=True, null=True, verbose_name='Subscription ends at')),
                ('position', models.TextField(blank=True, max_length=150, null=True, verbose_name='position')),
                ('country', models.CharField(blank=True, max_length=100, null=True, verbose_name='Country')),
                ('city', models.CharField(blank=True, max_length=100, null=True, verbose_name='City')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical profile',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Index',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Index In Ukraine')),
                ('name_ru', models.CharField(max_length=64, unique=True, verbose_name='Index In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='Index In English')),
                ('description', models.CharField(blank=True, max_length=512, verbose_name='Description In English')),
                ('description_ru', models.CharField(blank=True, max_length=512, verbose_name='Description In Russian')),
                ('description_ua', models.CharField(blank=True, max_length=512, verbose_name='Description In Ukraine')),
            ],
        ),
        migrations.CreateModel(
            name='LifeStyle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Life In Ukraine')),
                ('name_ru', models.CharField(max_length=64, unique=True, verbose_name='Life Style In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='Life Style In English')),
            ],
        ),
        migrations.CreateModel(
            name='Meals',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('proteins', models.IntegerField(blank=True, null=True, verbose_name='Proteins')),
                ('carbohydrates', models.IntegerField(blank=True, null=True, verbose_name='Carbohydrates')),
                ('fats', models.IntegerField(blank=True, null=True, verbose_name='Fats')),
                ('water', models.IntegerField(blank=True, null=True, verbose_name='Water')),
                ('calories', models.IntegerField(blank=True, null=True, verbose_name='Calories')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MealsPerDay',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Meals Per Day')),
                ('value', models.IntegerField(null=True, verbose_name='Value')),
            ],
        ),
        migrations.CreateModel(
            name='MealsPerDayName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Meals Per Day Name In Ukraine')),
                ('name_ru', models.CharField(max_length=64, verbose_name='Meals Per Day Name In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='Meals Per Day Name In English')),
                ('value', models.IntegerField(verbose_name='Value')),
                ('day_time', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='meals_per_day', to='recipe.RecipeDayTime', verbose_name='Recipe Day Time')),
                ('values', models.ManyToManyField(to='projectx.MealsPerDay')),
            ],
        ),
        migrations.CreateModel(
            name='PortionNumber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Portion Number Name')),
                ('value', models.IntegerField(verbose_name='Value')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('last_update', models.DateField(blank=True, null=True, verbose_name='Last Update')),
                ('change_time', models.BooleanField(default=False, verbose_name='Change Time')),
                ('team_member', models.BooleanField(default=False)),
                ('team_image', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Team Photo')),
                ('referral_code', models.CharField(blank=True, max_length=10, null=True, verbose_name='Referral Cope')),
                ('phone', models.CharField(blank=True, max_length=100, null=True, verbose_name='Phone')),
                ('file', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Photo')),
                ('info_form', models.BooleanField(default=False)),
                ('status', models.CharField(blank=True, max_length=100, null=True, verbose_name='Status')),
                ('name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Name')),
                ('first_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='First Name')),
                ('last_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Last Name')),
                ('name_slug', models.CharField(blank=True, max_length=150, null=True, verbose_name='Name Slug')),
                ('description_en', models.TextField(blank=True, max_length=800, null=True, verbose_name='Description In English')),
                ('description_ru', models.TextField(blank=True, max_length=800, null=True, verbose_name='Description In Russian')),
                ('description', models.TextField(blank=True, max_length=800, null=True, verbose_name='Description In Ukraine')),
                ('facebook', models.URLField(blank=True, null=True, verbose_name='Facebook')),
                ('instagram', models.URLField(blank=True, null=True, verbose_name='Instagram')),
                ('you_tube', models.URLField(blank=True, null=True, verbose_name='You_Tube')),
                ('slug', models.CharField(blank=True, help_text="Don't touch it!", max_length=150, verbose_name='Slug')),
                ('age', models.IntegerField(blank=True, null=True, verbose_name='Age')),
                ('height', models.IntegerField(blank=True, null=True, verbose_name='Height')),
                ('start_weight', models.IntegerField(blank=True, null=True, verbose_name='Start Weight')),
                ('weight', models.FloatField(blank=True, null=True, verbose_name='Weight')),
                ('subscription', models.BooleanField(default=False, verbose_name='Subscription')),
                ('bonus', models.CharField(blank=True, max_length=10, null=True, verbose_name='Bonus')),
                ('subscription_ends', models.DateTimeField(blank=True, null=True, verbose_name='Subscription ends at')),
                ('position', models.TextField(blank=True, max_length=150, null=True, verbose_name='position')),
                ('country', models.CharField(blank=True, max_length=100, null=True, verbose_name='Country')),
                ('city', models.CharField(blank=True, max_length=100, null=True, verbose_name='City')),
                ('body_index', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='projectx.Index', verbose_name='Index')),
                ('body_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='projectx.BodyType', verbose_name='Body Type')),
                ('delete_recipes', models.ManyToManyField(blank=True, related_name='delete_recipes', to='recipe.Recipe', verbose_name='Delete Recipes')),
                ('diet', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='projectx.Diet')),
                ('friend_request', models.ManyToManyField(blank=True, related_name='friend_request', to=settings.AUTH_USER_MODEL)),
                ('friends', models.ManyToManyField(blank=True, related_name='profile_friends', to=settings.AUTH_USER_MODEL)),
                ('life_style', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='projectx.LifeStyle', verbose_name='Life Style')),
                ('recipes', models.ManyToManyField(blank=True, to='recipe.Recipe')),
                ('referrals', models.ManyToManyField(blank=True, related_name='referrals', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Sex',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Sex In Ukraine')),
                ('name_ru', models.CharField(max_length=64, unique=True, verbose_name='Sex In Russian')),
                ('name_en', models.CharField(max_length=64, unique=True, verbose_name='Sex In English')),
            ],
        ),
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Target In Ukraine')),
                ('name_ru', models.CharField(max_length=64, unique=True, verbose_name='Target In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='Target In English')),
            ],
        ),
        migrations.CreateModel(
            name='UserStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='User Status In Ukraine')),
                ('name_ru', models.CharField(max_length=64, verbose_name='User Status In Russian')),
                ('name_en', models.CharField(blank=True, max_length=64, verbose_name='User Status In English')),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='sex',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='projectx.Sex', verbose_name='Sex'),
        ),
        migrations.AddField(
            model_name='profile',
            name='target',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='projectx.Target', verbose_name='Target'),
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
        migrations.AddField(
            model_name='profile',
            name='user_status',
            field=models.ForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.PROTECT, to='projectx.UserStatus', verbose_name='User Status'),
        ),
        migrations.AddField(
            model_name='meals',
            name='meals_per_day',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='projectx.MealsPerDayName', verbose_name='Meals Per Day Name'),
        ),
        migrations.AddField(
            model_name='meals',
            name='portion_number',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='projectx.PortionNumber', verbose_name='Portion Number'),
        ),
        migrations.AddField(
            model_name='meals',
            name='recipe',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='recipe.Recipe', verbose_name='Recipe'),
        ),
        migrations.AddField(
            model_name='meals',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='body_index',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.Index', verbose_name='Index'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='body_type',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.BodyType', verbose_name='Body Type'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='diet',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.Diet'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='life_style',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.LifeStyle', verbose_name='Life Style'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='sex',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.Sex', verbose_name='Sex'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='target',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.Target', verbose_name='Target'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
        migrations.AddField(
            model_name='historicalprofile',
            name='user_status',
            field=models.ForeignKey(blank=True, db_constraint=False, default=1, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projectx.UserStatus', verbose_name='User Status'),
        ),
    ]
