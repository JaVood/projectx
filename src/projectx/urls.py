from django.urls import re_path
from django.conf.urls import url
import django.views.defaults
from . import views

urlpatterns = [
    re_path(r'^$', views.IndexView.as_view(), name='index'),
    re_path(r'^contact/$', views.ContactView.as_view(), name='contact'),
    re_path(r'^private/$', views.PrivateView.as_view(), name='private'),
    re_path(r'^conditions/$', views.ConditionsView.as_view(), name='conditions'),
    re_path(r'^ru/private/$', views.PrivateRuView.as_view(), name='privateru'),
    re_path(r'^ru/conditions/$', views.ConditionsRuView.as_view(), name='conditionsru'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/$", views.profile, name='profile'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/diet/$", views.profile_diet, name='profile_diet'),

    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/category/(?P<meals_slug>.[\w-]+[']*)/(?P<category_slug>.[\w-]+[']*)/$", views.simple_category_recipe, name='simple_category_recipe'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/(?P<meals_slug>.[\w-]+[']*)/(?P<category_slug>.[\w-]+[']*)/(?P<recipe_slug>.+)/$", views.simple_recipe, name='simple_recipe'),

    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/friends/$", views.profile_friends, name='profile_friends'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/friends/search/$", views.friend_search, name='friend_search'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/recipes/$", views.profile_recipe, name='profile_recipe'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/recipes/search/$", views.recipe_search, name='recipe_search'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/recipes/(?P<category_slug>.[\w-]+[']*)/$", views.category_recipe, name='category_recipe'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/quick-decision/$", views.quick_decision, name='quick_decision'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/quick-decision/(?P<var>.[\w-]+[']*)/$", views.quick_decision_done, name='quick_decision_done'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/settings/$", views.profile_setting, name='profile_setting'),
    url(r"^profile/(?P<profile_slug>.[\w-]+[']*)/user/(?P<user_slug>.[\w-]+[']*)/$", views.profile_user, name='profile_user'),
    url(r'^form/$', views.infoform, name='info form'),
    re_path(r'^about/$', views.AboutView.as_view(), name='about'),
    re_path(r'^help/$', views.HelpView.as_view(), name='help'),

    re_path(r'^ru/$', views.IndexRuView.as_view(), name='indexru'),
    re_path(r'^ru/contact/$', views.ContactRuView.as_view(), name='contactru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/$", views.profileru, name='profileru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/diet/$", views.profile_dietru, name='profile_dietru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/category/(?P<meals_slug>.[\w-]+[']*)/(?P<category_slug>.[\w-]+[']*)/$",views.simple_category_reciperu, name='simple_category_reciperu'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/(?P<meals_slug>.[\w-]+[']*)/(?P<category_slug>.[\w-]+[']*)/(?P<recipe_slug>.+)/$",views.simple_reciperu, name='simple_reciperu'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/friends/$", views.profile_friendsru, name='profile_friendsru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/friends/search/$", views.friend_searchru, name='friend_searchru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/recipes/$", views.profile_reciperu, name='profile_reciperu'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/recipes/search/$", views.recipe_searchru, name='recipe_searchru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/recipes/(?P<category_slug>.[\w-]+[']*)/$", views.category_reciperu, name='category_reciperu'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/quick-decision/$", views.quick_decisionru, name='quick_decisionru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/quick-decision/(?P<var>.[\w-]+[']*)/$", views.quick_decision_doneru, name='quick_decision_doneru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/settings/$", views.profile_settingru, name='profile_settingru'),
    url(r"^ru/profile/(?P<profile_slug>.[\w-]+[']*)/user/(?P<user_slug>.[\w-]+[']*)/$", views.profile_userru, name='profile_userru',),
    url(r'^ru/form/$', views.infoformru, name='info form ru'),
    re_path(r'^ru/about/$', views.AboutRuView.as_view(), name='aboutru'),
    re_path(r'^ru/help/$', views.HelpRuView.as_view(), name='helpru'),
    url(r'^404/$', django.views.defaults.page_not_found, ),
    url(r'^pay-callback/$', views.PayCallbackView.as_view(), name='pay_callback'),
    url(r'^test/$', views.photo_list, name='test'),
]

