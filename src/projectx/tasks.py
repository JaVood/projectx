
from celery import Celery
from celery.schedules import crontab
from projectx.models import Profile

app = Celery('periodic', broker="pyamqp://guest@localhost//")
@app.task
def test():
    profile = Profile.objects.get(name='JaVood')
    profile.change_time = True
    profile.save()

app.conf.beat_schedule = {
    "test": {
        "task": "tasks.test",
        "schedule": crontab(minute="*"),
    }
}
