from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext as _
from projectx.generator import generate_diet
from core.models import BaseModel
import datetime
from simple_history.models import HistoricalRecords


class MealsPerDay(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name=_('Meals Per Day'),
    )

    value = models.IntegerField(verbose_name=_('Value'), null=True)

    def __str__(self):
        return self.name


class MealsPerDayName(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name=_('Meals Per Day Name In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        verbose_name=_('Meals Per Day Name In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Meals Per Day Name In English'),
    )

    values = models.ManyToManyField(MealsPerDay)

    value = models.IntegerField(verbose_name=_('Value'))

    slug = models.CharField(verbose_name='Slug', max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name


class Sex(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Sex In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Sex In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Sex In English'),
    )

    def __str__(self):
        return self.name


class LifeStyle(models.Model):
    name = models.CharField(
        max_length=128,
        unique=True,
        verbose_name=_('Life In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=128,
        unique=True,
        verbose_name=_('Life Style In Russian'),
    )

    name_en = models.CharField(
        max_length=128,
        blank=True,
        verbose_name=_('Life Style In English'),
    )

    def __str__(self):
        return self.name


class BodyType(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Body Type In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Body Type In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Body Type In English'),
    )

    description_en = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In English')
    )

    description_ru = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In Russian'),
    )

    description = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In Ukraine'),
    )

    def __str__(self):
        return self.name


class Index(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Index In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Index In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Index In English'),
    )

    description = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In English')
    )

    description_ru = models.CharField(
        max_length=512,
        verbose_name=_('Description In Russian'),
        blank=True,
    )

    description_ua = models.CharField(
        max_length=512,
        verbose_name=_('Description In Ukraine'),
        blank=True,
    )

    def __str__(self):
        return self.name


class Diet(BaseModel):
    boo = models.FloatField(verbose_name=_('BOO'), null=True)
    adaptation = models.FloatField(verbose_name=_('Adaptation'), null=True, default=0, blank=True)
    day_calories_norm = models.FloatField(verbose_name=_('Day Calories Norm'), null=True)

    day_target_calories = models.FloatField(verbose_name=_('Day Calories Target'), null=True)

    proteins = models.FloatField(verbose_name=_('Proteins'), null=True)

    carbohydrates = models.FloatField(verbose_name=_('Carbohydrates'), null=True)

    fats = models.FloatField(verbose_name=_('Fats'), null=True)

    water = models.IntegerField(verbose_name=_('Water'), null=True, default='2000')

    meals_per_day = models.IntegerField(verbose_name=_('Meals per day'), null=True)

    def __str__(self):
        if self.profile:
            return self.profile.user.username


DEFAULT_STATUS_ID = 1


class Profile(BaseModel):
    from recipe.models import Recipe
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_('User'))
    user_status = models.ForeignKey(
        to='UserStatus',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('User Status'),
        default=DEFAULT_STATUS_ID,
    )
    order_id = models.CharField(verbose_name=_('Order id'), max_length=255, null=True, blank=True)
    last_update = models.DateTimeField(verbose_name='Last Update', null=True, blank=True)
    change_time = models.BooleanField(verbose_name=_('Change Time'), default=False)
    team_member = models.BooleanField(default=False)
    team_image = models.ImageField(verbose_name='Team Photo', null=True, blank=True)
    referral_code = models.CharField(verbose_name=_('Referral Cope'), max_length=10, null=True, blank=True)
    referrals = models.ManyToManyField(User, related_name=_('referrals'), blank=True)
    phone = models.CharField(verbose_name=_('Phone'), max_length=100, null=True, blank=True)
    friends = models.ManyToManyField(User, related_name=_('profile_friends'), blank=True)
    friend_request = models.ManyToManyField(User, related_name=_('friend_request'), blank=True)
    file = models.ImageField(verbose_name='Photo', null=True, blank=True)
    info_form = models.BooleanField(default=False)
    status = models.CharField(verbose_name='Status', max_length=100, null=True, blank=True)
    name = models.CharField(verbose_name='Name', max_length=100, null=True, blank=True)
    first_name = models.CharField(verbose_name='First Name', max_length=100, null=True, blank=True)
    last_name = models.CharField(verbose_name='Last Name', max_length=100, null=True, blank=True)
    name_slug = models.CharField(verbose_name='Name Slug', max_length=150, null=True, blank=True)
    description_en = models.TextField(
        max_length=2000,
        blank=True,
        null=True,
        verbose_name=_('Description In English')
    )

    description_ru = models.TextField(
        max_length=2000,
        verbose_name=_('Description In Russian'),
        blank=True,
        null=True,
    )

    description = models.TextField(
        max_length=2000,
        verbose_name=_('Description In Ukraine'),
        blank=True,
        null=True,
    )

    facebook = models.URLField(verbose_name=_('Facebook'), blank=True, null=True)
    instagram = models.URLField(verbose_name=_('Instagram'), blank=True, null=True)
    you_tube = models.URLField(verbose_name=_('You_Tube'), blank=True, null=True)

    slug = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )
    sex = models.ForeignKey(
        to='Sex',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Sex'),
    )
    age = models.IntegerField(null=True, blank=True, verbose_name=_('Age'),)
    growth = models.IntegerField(null=True, blank=True, verbose_name=_('Height'),)
    start_weight = models.IntegerField(null=True, blank=True, verbose_name=_('Start Weight'),)
    weight = models.FloatField(null=True, blank=True, verbose_name=_('Weight'),)
    target = models.ForeignKey(
        to='Target',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Target'),
    )
    life_style = models.ForeignKey(
        to='LifeStyle',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Life Style'),
    )
    body_type = models.ForeignKey(
        to='BodyType',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Body Type'),
    )
    body_index = models.ForeignKey(
        to='Index',
        on_delete=models.CASCADE,
        verbose_name=_('Index'),
        null=True,
        blank=True,
    )

    diet = models.OneToOneField(
        to='Diet',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    recipes = models.ManyToManyField(Recipe, blank=True)

    delete_recipes = models.ManyToManyField(Recipe,
                                            blank=True,
                                            verbose_name=_('Delete Recipes'),
                                            related_name=_('delete_recipes')
                                            )

    subscription = models.BooleanField(default=False, verbose_name=_('Subscription'),)

    bonus = models.CharField(verbose_name=_('Bonus'), null=True, blank=True, max_length=20)

    subscription_ends = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_('Subscription ends at')
    )

    short_description = models.TextField(max_length=300, verbose_name=_('Short Description In Ukraine'), blank=True, null=True)
    short_description_ru = models.TextField(max_length=300, verbose_name=_('Short Description In Russian'), blank=True, null=True)
    short_description_en = models.TextField(max_length=300, verbose_name=_('Short Description In English'), blank=True, null=True)
    country = models.CharField(verbose_name='Country', max_length=100, null=True, blank=True)

    city = models.CharField(verbose_name='City', max_length=100, null=True, blank=True)

    history = HistoricalRecords()

    @property
    def is_expired(self):
            if datetime.datetime.now() > self.subscription_ends:
                self.subscription = False
            return False

    def save(self, *args, **kwargs):
        if self.start_weight is None and self.weight:
            self.start_weight = self.weight
        if self.first_name is None:
            self.first_name = self.user.first_name
        if self.last_name is None:
            self.last_name = self.user.last_name
        if self.sex_id is None:
            pass
        elif self.diet is None:
            diet = Diet(profile=self)
            diet.save()
            self.diet = diet
            generate_diet(self)
        else:
            generate_diet(self)
        if self.bonus:
            self.bonus = self.bonus.lower()
        super(Profile, self).save(*args, **kwargs)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
        instance.profile.save()

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return self.user.username


class Target(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Target In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Target In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Target In English'),
    )

    def __str__(self):
        return self.name


class PortionNumber(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name=_('Portion Number Name'),
    )

    value = models.IntegerField(verbose_name=_('Value'))

    def __str__(self):
        return self.name


class Meals(BaseModel):
    from recipe.models import Recipe, FSRecipe
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             verbose_name=_('User'),
                             )

    meals_per_day = models.ForeignKey(MealsPerDayName,
                                      on_delete=models.SET_NULL,
                                      verbose_name=_('Meals Per Day Name'),
                                      null=True
                                      )

    portion_number = models.ForeignKey(PortionNumber,
                                       on_delete=models.CASCADE,
                                       verbose_name=_('Portion Number'),
                                       null=True,
                                       )

    recipe = models.ForeignKey(Recipe,
                               on_delete=models.SET_NULL,
                               verbose_name=_('Recipe'),
                               null=True,
                               blank=True
                               )

    fsrecipe = models.ForeignKey(FSRecipe,
                                 on_delete=models.SET_NULL,
                                 verbose_name=_('FSRecipe'),
                                 null=True,
                                 blank=True
                                 )

    proteins = models.IntegerField(verbose_name=_('Proteins'), null=True, blank=True)

    carbohydrates = models.IntegerField(verbose_name=_('Carbohydrates'), null=True, blank=True)

    fats = models.IntegerField(verbose_name=_('Fats'), null=True, blank=True)

    water = models.IntegerField(verbose_name=_('Water'), null=True, blank=True)

    calories = models.IntegerField(verbose_name=_('Calories'), null=True, blank=True)

    portion = models.IntegerField(verbose_name=_('Portion'), null=True, blank=True)

    def __str__(self):
        return self.user.username


class UserStatus(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name=_('User Status In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        verbose_name=_('User Status In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('User Status In English'),
    )

    def __str__(self):
        return self.name


class Bonus(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             verbose_name=_('User'),
                             )

    value = models.CharField(verbose_name=_('Value'), max_length=20)
    discount = models.IntegerField(verbose_name=_('Discount'), default=0)

    def __str__(self):
        return self.user.username

