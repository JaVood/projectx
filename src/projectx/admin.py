from django.contrib import admin
from projectx import models
from projectx import forms
from simple_history import admin as aadmin


@admin.register(models.Profile)
class ProfileAdmin(aadmin.SimpleHistoryAdmin):
    list_display = ['user', 'user_status', 'subscription_ends']
    list_filter = ['user_status', 'subscription']
    form = forms.ProfileForm
    readonly_fields = ('created', 'updated', 'diet', 'body_index')


@admin.register(models.Sex)
class SexAdmin(admin.ModelAdmin):
    form = forms.SexForm


@admin.register(models.BodyType)
class BodyAdmin(admin.ModelAdmin):
    form = forms.BodyTypeForm


@admin.register(models.UserStatus)
class UserStatusAdmin(admin.ModelAdmin):
    form = forms.UserStatusForm


@admin.register(models.Target)
class BodyAdmin(admin.ModelAdmin):
    form = forms.TargetForm


@admin.register(models.LifeStyle)
class LifeAdmin(admin.ModelAdmin):
    form = forms.LifeStyleForm


@admin.register(models.Diet)
class DietGroupAdmin(admin.ModelAdmin):
    form = forms.DietForm
    readonly_fields = ('boo',
                       'day_calories_norm',
                       'day_target_calories',
                       'proteins',
                       'carbohydrates',
                       'fats',
                       'meals_per_day',
                       'created',
                       'updated'
                       )


@admin.register(models.Index)
class IndexGroupAdmin(admin.ModelAdmin):
    form = forms.IndexForm


@admin.register(models.MealsPerDay)
class MealsPerDayAdmin(admin.ModelAdmin):
    form = forms.MealsPerDayForm


@admin.register(models.MealsPerDayName)
class MealsPerDayNameAdmin(admin.ModelAdmin):
    form = forms.MealsPerDayNameForm


@admin.register(models.Meals)
class MealsAdmin(admin.ModelAdmin):
    list_display = ['user', 'meals_per_day', 'created']
    list_filter = ['user', 'created']
    form = forms.MealsForm


@admin.register(models.PortionNumber)
class PortionNumberAdmin(admin.ModelAdmin):
    form = forms.PortionNumberForm


@admin.register(models.Bonus)
class BonusAdmin(admin.ModelAdmin):
    form = forms.BonusForm
