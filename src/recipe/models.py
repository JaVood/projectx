from django.db import models
from core.models import BaseModel
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from projectx.models import MealsPerDayName


class Recipe(BaseModel):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Name In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Name In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Recipe Name In English'),
    )

    author = models.ForeignKey(
        User,
        related_name='recipe',
        on_delete=models.PROTECT,
        verbose_name=_('Author'),
    )

    slug = models.CharField(
        max_length=150,
        unique=True,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    image = models.ImageField(verbose_name='Image', blank=True, null=True)

    recipe_category = models.ForeignKey(
        to='RecipeCategory',
        related_name='recipe',
        on_delete=models.PROTECT,
        verbose_name=_('Recipe Category'),
    )

    description = models.CharField(
        max_length=512,
        verbose_name=_('Description In Ukraine'),
        blank=True,
    )

    description_ru = models.CharField(
        max_length=512,
        verbose_name=_('Description In Russian'),
        blank=True,
    )

    description_en = models.CharField(
        max_length=512,
        blank=True,
        verbose_name=_('Description In English')
    )

    products = models.TextField(max_length=2000, verbose_name=_('Products'), blank=True, null=True)

    products_ru = models.TextField(max_length=2000, verbose_name=_('Products Ru'), blank=True, null=True)

    products_en = models.TextField(max_length=2000, verbose_name=_('Products En'), blank=True, null=True)

    cooking_method = models.TextField(max_length=2000, verbose_name=_('Cooking Method'), blank=True, null=True)

    cooking_method_ru = models.TextField(max_length=2000, verbose_name=_('Cooking Method Ru'), blank=True, null=True)

    cooking_method_en = models.TextField(max_length=2000, verbose_name=_('Cooking Method En'), blank=True, null=True)

    day_time = models.ManyToManyField(MealsPerDayName)

    portion = models.IntegerField(verbose_name=_('Portion'))

    portion_data = models.ForeignKey(
        to='RecipePortionData',
        related_name='recipe',
        on_delete=models.PROTECT,
        verbose_name=_('Recipe Portion Data'),
    )

    proteins = models.IntegerField(verbose_name=_('Proteins 1p'))

    carbohydrates = models.IntegerField(verbose_name=_('Carbohydrates 1p'))

    fats = models.IntegerField(verbose_name=_('Fats 1p'))

    calories = models.IntegerField(verbose_name=_('Calories 1p'), default='0')

    water = models.IntegerField(verbose_name=_('Water'), default='0')

    def __str__(self):
        return self.name


class RecipeCategory(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Category In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Category In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Recipe Category In English'),
    )

    image = models.ImageField(verbose_name=_('Image'), null=True, blank=True)

    description = models.CharField(
        max_length=512,
        verbose_name=_('Description In Ukraine'),
        blank=True,
    )
    description_ru = models.CharField(
        max_length=512,
        verbose_name=_('Description In Russian'),
        blank=True,
    )

    description_en = models.CharField(
        max_length=512,
        verbose_name=_('Description In English'),
        blank=True,
    )

    slug = models.CharField(
        max_length=150,
        unique=True,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    def __str__(self):
        return self.name


class RecipePortionData(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Portion Data In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Portion Data In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Recipe Portion Data In English'),
    )

    def __str__(self):
        return self.name


class FSRecipe(BaseModel):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Name In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Name In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Recipe Name In English'),
    )

    author = models.ForeignKey(
        User,
        related_name='fsrecipe',
        on_delete=models.PROTECT,
        verbose_name=_('Author'),
    )

    slug = models.CharField(
        max_length=150,
        unique=True,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    image = models.ImageField(verbose_name='Image', blank=True, null=True)

    recipe_category = models.ForeignKey(
        to='FSRecipeCategory',
        related_name='recipe',
        on_delete=models.PROTECT,
        verbose_name=_('FS Recipe Category'),
    )

    products = models.TextField(max_length=2000, verbose_name=_('Products'), blank=True, null=True)

    products_ru = models.TextField(max_length=2000, verbose_name=_('Products Ru'), blank=True, null=True)

    products_en = models.TextField(max_length=2000, verbose_name=_('Products En'), blank=True, null=True)

    cooking_method = models.TextField(max_length=2000, verbose_name=_('Cooking Method'), blank=True, null=True)

    cooking_method_ru = models.TextField(max_length=2000, verbose_name=_('Cooking Method Ru'), blank=True, null=True)

    cooking_method_en = models.TextField(max_length=2000, verbose_name=_('Cooking Method En'), blank=True, null=True)

    day_time = models.ManyToManyField(MealsPerDayName)

    portion = models.IntegerField(verbose_name=_('Portion'))

    portion_data = models.ForeignKey(
        to='RecipePortionData',
        related_name='fsrecipe',
        on_delete=models.PROTECT,
        verbose_name=_('Recipe Portion Data'),
    )

    proteins = models.IntegerField(verbose_name=_('Proteins 1p'))

    carbohydrates = models.IntegerField(verbose_name=_('Carbohydrates 1p'))

    fats = models.IntegerField(verbose_name=_('Fats 1p'))

    calories = models.IntegerField(verbose_name=_('Calories 1p'), default='0')

    water = models.IntegerField(verbose_name=_('Water'), default='0')

    recommended = models.BooleanField(verbose_name=_('Recommended'))

    def __str__(self):
        return self.name


class FSRecipeCategory(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Category In Ukraine'),
    )

    name_ru = models.CharField(
        max_length=64,
        unique=True,
        verbose_name=_('Recipe Category In Russian'),
    )

    name_en = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_('Recipe Category In English'),
    )

    image = models.ImageField(verbose_name=_('Image'), null=True, blank=True)

    slug = models.CharField(
        max_length=150,
        unique=True,
        blank=True,
        verbose_name=_('Slug'),
        help_text="Don't touch it!"
    )

    def __str__(self):
        return self.name
