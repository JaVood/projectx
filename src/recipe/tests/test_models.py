from django.contrib.auth import get_user_model
from django.test import TestCase
from recipe.models import Recipe, RecipeCategory, RecipeDayTime, RecipeDifficult, RecipePortionData
from projectx.models import UserStatus


class BaseTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username='user', password='qwerty123'
        )

        UserStatus.objects.create(
            id="1",
            name="test",
        )

        RecipePortionData.objects.create(
            id="1",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        RecipeDayTime.objects.create(
            id="2",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        RecipeDifficult.objects.create(
            id="3",
            name="test",
            name_ru="test2",
            name_en="test3"
        )

        RecipeCategory.objects.create(
            id="4",
            name="test",
            name_ru="test2",
            name_en="test3",
            description="test",
            description_ru="test",
            description_en="test",
            slug="test",
        )

        Recipe.objects.create(
            id="5",
            name="test",
            name_ru="test2",
            name_en="test3",
            slug="test",
            author=self.user,
            description="Run it",
            description_ru="Run it",
            description_en="Run it",
            recipe_category_id="4",
            products="products",
            products_ru="products",
            products_en="products",
            cooking_method="cooking",
            cooking_method_ru="cooking",
            cooking_method_en="cooking",
            difficult_id="3",
            day_time_id="2",
            portion="100",
            portion_data_id="1",
            proteins="100",
            carbohydrates="100",
            fats="100",
            calories="100",
            likes="100",
            water="100"
        )


class RecipePortionModelTest(BaseTest):
    def test_recipe_portion_name(self):
        recipe_portion = RecipePortionData.objects.get(id=1)
        self.assertEqual(recipe_portion.name, "test")

    def test_recipe_portion_name_ru(self):
        recipe_portion = RecipePortionData.objects.get(id=1)
        self.assertEqual(recipe_portion.name_ru, "test2")

    def test_recipe_portion_name_en(self):
        recipe_portion = RecipePortionData.objects.get(id=1)
        self.assertEqual(recipe_portion.name_en, "test3")


class RecipeDayTimeModelTest(BaseTest):
    def test_recipe_day_time_name(self):
        recipe_day_time = RecipeDayTime.objects.get(id=2)
        self.assertEqual(recipe_day_time.name, "test")

    def test_recipe_day_time_name_ru(self):
        recipe_day_time = RecipeDayTime.objects.get(id=2)
        self.assertEqual(recipe_day_time.name_ru, "test2")

    def test_recipe_day_time_name_en(self):
        recipe_day_time = RecipeDayTime.objects.get(id=2)
        self.assertEqual(recipe_day_time.name_en, "test3")


class RecipeDifficultModelTest(BaseTest):
    def test_recipe_difficult_name(self):
        recipe_difficult = RecipeDifficult.objects.get(id=3)
        self.assertEqual(recipe_difficult.name, "test")

    def test_recipe_difficult_name_ru(self):
        recipe_difficult = RecipeDifficult.objects.get(id=3)
        self.assertEqual(recipe_difficult.name_ru, "test2")

    def test_recipe_difficult_name_en(self):
        recipe_difficult = RecipeDifficult.objects.get(id=3)
        self.assertEqual(recipe_difficult.name_en, "test3")


class RecipeCategoryModelTest(BaseTest):
    def test_recipe_category_name(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.name, "test")

    def test_recipe_category_name_ru(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.name_ru, "test2")

    def test_recipe_category_name_en(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.name_en, "test3")

    def test_recipe_category_description(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.description, "test")

    def test_recipe_category_description_ru(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.description_ru, "test")

    def test_recipe_category_description_en(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.description_en, "test")

    def test_recipe_category_slug(self):
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe_category.slug, "test")


class RecipeModelTest(BaseTest):
    def test_recipe_name(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.name, "test")

    def test_recipe_name_ru(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.name_ru, "test2")

    def test_recipe_name_en(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.name_en, "test3")

    def test_recipe_description(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.description, "Run it")

    def test_recipe_description_ru(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.description_ru, "Run it")

    def test_recipe_description_en(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.description_en, "Run it")

    def test_recipe_slug(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.slug, "test")

    def test_recipe_user(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.author, self.user)

    def test_recipe_category(self):
        recipe = Recipe.objects.get(id=5)
        recipe_category = RecipeCategory.objects.get(id=4)
        self.assertEqual(recipe.recipe_category, recipe_category)

    def test_recipe_products(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.products, "products")

    def test_recipe_products_ru(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.products_ru, "products")

    def test_recipe_products_en(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.products_en, "products")

    def test_recipe_cooking_method(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.cooking_method, "cooking")

    def test_recipe_cooking_method_ru(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.cooking_method_ru, "cooking")

    def test_recipe_cooking_method_en(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.cooking_method_en, "cooking")

    def test_recipe_difficult(self):
        recipe = Recipe.objects.get(id=5)
        recipe_difficult = RecipeDifficult.objects.get(id=3)
        self.assertEqual(recipe.difficult, recipe_difficult)

    def test_recipe_day_time(self):
        recipe = Recipe.objects.get(id=5)
        recipe_day_time = RecipeDayTime.objects.get(id=2)
        self.assertEqual(recipe.day_time, recipe_day_time)

    def test_recipe_portion(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.portion, 100)

    def test_recipe_portion_data(self):
        recipe = Recipe.objects.get(id=5)
        recipe_portion_data = RecipePortionData.objects.get(id=1)
        self.assertEqual(recipe.portion_data, recipe_portion_data)

    def test_recipe_proteins(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.proteins, 100)

    def test_recipe_calories(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.calories, 100)

    def test_recipe_carbohydrates(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.carbohydrates, 100)

    def test_recipe_fats(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.fats, 100)

    def test_recipe_likes(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.likes, 100)

    def test_recipe_water(self):
        recipe = Recipe.objects.get(id=5)
        self.assertEqual(recipe.water, 100)