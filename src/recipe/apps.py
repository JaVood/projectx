from django.apps import AppConfig
from watson import search as watson


class RecipeConfig(AppConfig):
    name = 'recipe'

    def ready(self):
        Recipe = self.get_model("Recipe")
        watson.register(Recipe)
