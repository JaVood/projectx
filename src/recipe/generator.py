def count_cardo(user):
    from projectx.models import Diet

    diet = Diet.objects.get(id=user.diet_id)

    # count meals per day with carbohydrates
    if diet.meals_per_day >= 4 and user.target == 1:
        with_carbo = diet.meals_per_day - 2
    elif diet.meals_per_day < 4 and user.target == 1 or user.target == 3:
        with_carbo = diet.meals_per_day - 1
    else:
        with_carbo = diet.meals_per_day

    return with_carbo
