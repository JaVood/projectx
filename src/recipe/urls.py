from django.conf.urls import url


from . import views

urlpatterns = [
    url(r"^ru/(?P<recipe_slug>.+)/$", views.reciperu, name='reciperu'),
    url(r"^(?P<recipe_slug>.+)/$", views.recipe, name='recipe'),
]
