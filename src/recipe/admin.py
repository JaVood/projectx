from django.contrib import admin
from recipe import models
from recipe import forms


@admin.register(models.Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ['name', 'recipe_category']
    list_filter = ['recipe_category']
    form = forms.RecipeForm
    readonly_fields = ('created',
                       'updated'
                       )


@admin.register(models.RecipeCategory)
class RecipeCategoryAdmin(admin.ModelAdmin):
    form = forms.RecipeCategoryForm


@admin.register(models.RecipePortionData)
class RecipePortionDataAdmin(admin.ModelAdmin):
    form = forms.RecipePortionDataForm


@admin.register(models.FSRecipe)
class FSRecipeAdmin(admin.ModelAdmin):
    list_display = ['name', 'recipe_category']
    list_filter = ['recipe_category']
    form = forms.RecipeForm
    readonly_fields = ('created',
                       'updated'
                       )


@admin.register(models.FSRecipeCategory)
class FSRecipeCategoryAdmin(admin.ModelAdmin):
    form = forms.FSRecipeCategoryForm

