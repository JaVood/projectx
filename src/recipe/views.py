from core.views import BaseView
from django.shortcuts import render, redirect
from projectx.models import Profile, MealsPerDayName
from django.shortcuts import get_object_or_404
from recipe.models import Recipe
from django_private_chat.models import Dialog
from django.http import Http404, JsonResponse
from django.template.loader import render_to_string
from projectx.models import MealsPerDay
from projectx.forms import MealsForm, SavedRecipeForm
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def recipe(request, recipe_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/form')
    else:
        return redirect('/login')
    profile = Profile.objects.get(user_id=user.id)
    recipe = get_object_or_404(Recipe, slug=recipe_slug)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    recommend = profile.diet.day_target_calories / meals
    messages = 0
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')
    if request.method == "POST":

        form = MealsForm(request.POST)
        save = SavedRecipeForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_diet', profile_slug=profile.slug)
        elif save.is_valid():
            rec = save.cleaned_data['recipes']
            for i in rec:
                profile.recipes.add(i)
                profile.delete_recipes.remove(i)
                return redirect('recipe:recipe', recipe_slug=recipe.slug)

    else:
        form = MealsForm()
        save = SavedRecipeForm()
    context = ({'form': form,
                'save': save,
                'profile': profile,
                'recipe': recipe,
                'friend_request': friend_request,
                'messages': messages,
                'meals': meals_name,
                'recommend': int(recommend),
                })
    if request.is_ajax():
        rendered = render_to_string('recipe/recipe.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'recipe/recipe.html', context)


@csrf_exempt
def reciperu(request, recipe_slug):
    user = request.user
    if user.is_authenticated:
        if request.user.profile.info_form == False:
            return redirect('/ru/form')
    else:
        return redirect('/ru/login')
    profile = Profile.objects.get(user_id=user.id)
    recipe = get_object_or_404(Recipe, slug=recipe_slug)
    meals = profile.diet.meals_per_day
    meals_name = MealsPerDay.objects.get(value=meals)
    meals_name = meals_name.mealsperdayname_set.all()
    dialogs_1 = Dialog.objects.filter(opponent_id=user.id)
    dialogs_2 = Dialog.objects.filter(owner_id=user.id)
    messages = 0
    recommend = profile.diet.day_target_calories / meals
    for d in dialogs_1:
        d.no_read = 0
        d.save()
    for d in dialogs_2:
        d.no_read = 0
        d.save()
    for d in dialogs_1:
        for i in d.messages.all():
            if i.read == False and i.sender != user:
                messages += 1
    friend_request = profile.friend_request.all().order_by('id')
    if request.method == "POST":

        form = MealsForm(request.POST)
        save = SavedRecipeForm(request.POST)
        if form.is_valid():
            form.save()
            meal = form.save(commit=False)
            meal_recipe = Recipe.objects.get(id=meal.recipe_id)
            meal.proteins = meal_recipe.proteins * meal.portion_number_id
            meal.carbohydrates = meal_recipe.carbohydrates * meal.portion_number_id
            meal.fats = meal_recipe.fats * meal.portion_number_id
            meal.water = meal_recipe.water * meal.portion_number_id
            meal.calories = meal_recipe.calories * meal.portion_number_id
            meal.save()
            return redirect('projectx:profile_dietru', profile_slug=profile.slug)
        elif save.is_valid():
            rec = save.cleaned_data['recipes']
            for i in rec:
                profile.recipes.add(i)
                profile.delete_recipes.remove(i)
                return redirect('recipe:reciperu', recipe_slug=recipe.slug)

    else:
        form = MealsForm()
        save = SavedRecipeForm()
    context = ({'form': form,
                'save': save,
                'profile': profile,
                'recipe': recipe,
                'friend_request': friend_request,
                'messages': messages,
                'meals': meals_name,
                'recommend': int(recommend),
                })
    if request.is_ajax():
        rendered = render_to_string('recipe/recipe-ru.html', context)
        response = {'html': rendered}
        return JsonResponse(response)
    else:
        return render(request, 'recipe/recipe-ru.html', context)

