from django import forms
from recipe import models
from transliterate.exceptions import LanguageDetectionError
import re
from transliterate import translit


def transliterate(text):
    pieces = str(re.sub('[\W]+', ' ', text)).lower().split(' ')
    result = []

    for piece in pieces:
        try:
            result.append(translit(piece, reversed=True))
        except LanguageDetectionError:
            result.append(piece)
    return '-'.join([r for r in result if r])


class RecipeForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.Recipe
        fields = '__all__'


class RecipeCategoryForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.RecipeCategory
        fields = '__all__'


class RecipePortionDataForm(forms.ModelForm):
    class Meta:
        model = models.RecipePortionData
        fields = '__all__'


class FSRecipeForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.FSRecipe
        fields = '__all__'


class FSRecipeCategoryForm(forms.ModelForm):
    def clean_slug(self):
        return transliterate(self.cleaned_data['name'])

    class Meta:
        model = models.FSRecipeCategory
        fields = '__all__'

