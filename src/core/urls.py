"""ProjectX URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path, re_path
from django.contrib.auth.views import LogoutView
from projectx import views as core_views
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from . import views


admin.site.index_title = 'ProjectX administration'

urlpatterns = [
    path('boss/', admin.site.urls),
    url(r'^ru/account/$', views.login_redirect_ru, name='login_redirect_ru'),
    url(r'^account/$', views.login_redirect, name='login_redirect'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^chat/', include('django_private_chat.urls')),
    re_path(r'', include(('projectx.urls', 'projectx'), namespace='projectx')),
    re_path(r'^blog/', include(('blog.urls', 'blog'), namespace='blog')),
    re_path(r'^recipe/', include(('recipe.urls', 'recipe'), namespace='recipe')),
    re_path(r'^logout/$', LogoutView.as_view(), name='logout'),
    re_path(r'^logout/ru/$', views.LogoutRuView.as_view(), name='logout_ru'),
    url(r'^login/',
        auth_views.LoginView.as_view(redirect_authenticated_user=True),
        name='login'),
    url(r'^ru/login/',
        views.LoginRuView.as_view(redirect_authenticated_user=True),
        name='login'),
    url(r'^signup/$', core_views.signup, name='signup'),
    url(r'^ru/signup/$', core_views.signupru, name='signupru'),



    path('ru/password_change/', views.PasswordChangeRuView.as_view(), name='password_change_ru'),
    path('ru/password_change/done/', views.PasswordChangeRuDoneView.as_view(), name='password_change_done_ru'),

    path('ru/password_reset/', views.PasswordResetRuView.as_view(), name='password_reset_ru'),
    path('ru/password_reset/done/', views.PasswordResetDoneRuView.as_view(), name='password_reset_done_ru'),
    path('ru/reset/<uidb64>/<token>/', views.PasswordResetConfirmRuView.as_view(), name='password_reset_confirm_ru'),
    path('ru/reset/done/', views.PasswordResetCompleteRuView.as_view(), name='password_reset_complete_ru'),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
