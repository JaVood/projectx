from celery.task import periodic_task
from celery.schedules import crontab
from datetime import date
from projectx.models import Profile
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from quick_decision import models


@periodic_task(
    run_every=(crontab(hour='1', minute='0')),
    name="change_time",
    ignore_result=True
)
def change_time():
    profile = Profile.objects.all()
    for i in profile:
        if timezone.now() > i.last_update + relativedelta(weeks=1):
            i.change_time = True
            i.save()

@periodic_task(
    run_every=(crontab(hour='1', minute='0')),
    name="delete_var",
    ignore_result=True
)
def delete_var():
    var = models.Var.objects.all()

    for i in var:
        i.delete()

@periodic_task(
    run_every=(crontab(hour='1', minute='0')),
    name="check_subsription",
    ignore_result=True
)
def check_subsription():
    profile = Profile.objects.all()

    for i in profile:
        if i.subscription_ends != None:
            if timezone.now() > i.subscription_ends:
                i.subscription = False
                i.user_status_id = 1
                i.save()
